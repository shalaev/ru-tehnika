<?php
/**
 * PHPExcel
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2012 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.8, 2012-10-12
 */

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

# задаем параметры базы данных
$dbhost = "localhost"; // хост
$dbname = "u10292"; // имя БД
$dbuser = "u10292"; // имя польз-ля
$dbpass = "vodag1potah"; // пароль к БД

mysql_connect($dbhost,$dbuser,$dbpass);
mysql_select_db($dbname);
mysql_set_charset( 'utf8' ); 

# подключаем класс (в каталоге CORE)
require_once '../CORE/classes/PHPExcel.php';
include_once '../CORE/classes/PHPExcel/IOFactory.php';

//соединяем с базой данных
$date = date("Y-m-d");
$query = "SELECT * FROM mp_catalog_items ORDER by `parent`, `order`";
if ($result = mysql_query($query) or die(mysql_error())) {

//создаем новый PHPExcel объект
$objPHPExcel = new PHPExcel();
//устанавливаем метаданные
$objPHPExcel->getProperties()->setCreator("PHP")
		->setLastModifiedBy("Инком Энерго")
		->setTitle('Данные с сайта за '.$date.'');
$objPHPExcel->getActiveSheet()->setTitle(''.$date.''); 
//устанавливаем ширину
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(22);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(26);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);

//пока непонятно зачем эта херь
$headerFill = new PHPExcel_Style_Fill;
$headerFill->setFillType( 'NONE' );

//настройки для шрифтов
$boldFont = array(
	'font'=>array(
	'bold'=>true
	)
);

$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->applyFromArray($boldFont);

$rowNumber = 1;
$headings = array('id', 'available', 'url', 'price', 'currencyId', 'category', 'picture', 'name', 'description'); 
$objPHPExcel->getActiveSheet()->fromArray(array($headings),NULL,'A'.$rowNumber); 
$rowNumber++;		

$rowNumber2 = 2;
if (mysql_num_rows($result) > 0) {
	while($row = mysql_fetch_array($result)) // берем результаты из каждого ряда
	{
		// Наличие на складе
		if ($row['in_stock'] == '1') {
			$stock = 'true';
		} else { 
			$stock = 'false'; }		
	
		// Получаем УРЛЫ
		$url = 'http://www.ru-tehnika.ru/catalog/'.$row['parent'].'/'.$row['id'];
		
		// Получаем названия каталогов
		if ($row['parent'] == '6') {
			$catalog = 'Дизельные электростанции';
		}
		if ($row['parent'] == '4') {
			$catalog = 'Дизельные электростанции';
		}		
		if ($row['parent'] == '5') {
			$catalog = 'Дизельные электростанции';
		}
		if ($row['parent'] == '7') {
			$catalog = 'Дизельные электростанции';
		}
		if ($row['parent'] == '13') {
			$catalog = 'Дизельные электростанции';
		}
		if ($row['parent'] == '14') {
			$catalog = 'Дизельные электростанции';
		}
		if ($row['parent'] == '15') {
			$catalog = 'Дизельные электростанции';
		}		
		if ($row['parent'] == '8') {
			$catalog = 'Блок-контейнеры';
		}
		if ($row['parent'] == '11') {
			$catalog = 'Шкафы АВР, ШУДГ, ШУПТ';
		}

		// Узнаем УРЛ картинки
		$images = !empty($row['images']) ? unserialize($row['images']) : false;
		$image_url = '';

		if ($images)
		{
			foreach ($images as $num=>$img)
			{
				if ($num==0) $image_url = 'http://www.master-elec.ru'.$img['path'][3];
			}
		}		
		
		$objPHPExcel->getActiveSheet()->fromArray(array($row['id']),NULL,'A'.$rowNumber2);
		$objPHPExcel->getActiveSheet()->fromArray(array($stock),NULL,'B'.$rowNumber2);
		$objPHPExcel->getActiveSheet()->fromArray(array($url),NULL,'C'.$rowNumber2);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$rowNumber2, 'RUR');
		$objPHPExcel->getActiveSheet()->fromArray(array($catalog),NULL,'F'.$rowNumber2);
		$objPHPExcel->getActiveSheet()->fromArray(array($image_url),NULL,'G'.$rowNumber2);
		$objPHPExcel->getActiveSheet()->fromArray(array(strip_tags($row['title_menu'])),NULL,'H'.$rowNumber2);
		$objPHPExcel->getActiveSheet()->fromArray(array(strip_tags($row['desc_page'])),NULL,'I'.$rowNumber2);
		$rowNumber2++;
	}
}

// Save as an Excel BIFF (xls) file
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Продукция.xls"');
header('Cache-Control: max-age=0');

$objWriter->save('php://output');
exit();
}
echo 'Нет данных в БД'; 

