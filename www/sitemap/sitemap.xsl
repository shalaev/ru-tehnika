<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
                xmlns:html="http://www.w3.org/TR/REC-html40"
                xmlns:sitemap="http://www.sitemaps.org/schemas/sitemap/0.9"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<title>XML Sitemap</title>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<style type="text/css">

					body {
						font-family:"Lucida Grande","Lucida Sans Unicode",Tahoma,Verdana;
						font-size:13px;
					}
					
					#intro {
						background-color:#eee;
						border:1px #990000 solid;
						padding:5px 13px 5px 13px;
						margin:10px;
					}
					
					#intro p {
						line-height:	16.8667px;
					}
					
					td {
						font-size:11px;
					}
					
					th {
						text-align:left;
						padding-right:30px;
						font-size:11px;
					}
					
					tr.high {
						background-color:whitesmoke;
					}
					
					#footer {
						padding:2px;
						margin:10px;
						font-size:8pt;
						color:gray;
					}
					
					#footer a {
						color:gray;
					}
					
					a {
						color:black;
					}
				</style>
			</head>
			<body>
				<h1>XML Sitemap</h1>
				<div id="intro">
					<p>
						Это XML Sitemap, который будет обрабатываться популярными поисковыми системами как <a href="http://www.google.ru">Google</a>, <a href="http://www.yandex.ru">Yandex</a>, <a href="http://search.msn.com">MSN Search</a> и <a href="http://www.yahoo.com">YAHOO</a>.<br />

						Авторы Sitemap XML: <a href="http://www.odblog.ru/">Дмитрий Охотников</a>, и <a href="http://www.arnebrachhold.de/">Arne Brachhold</a>.<br />
						Вы можете найти более подробную информацию о XML картах сайта на <a href="http://sitemaps.org">sitemaps.org</a>.
					</p>

				</div>
				<div id="content">
					<table cellpadding="5">
						<tr style="border-bottom:1px black solid;">
							<th>URL</th>
							<th>Приоритет</th>
						</tr>
						<xsl:variable name="lower" select="'abcdefghijklmnopqrstuvwxyz'"/>
						<xsl:variable name="upper" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
						<xsl:for-each select="sitemap:urlset/sitemap:url">
							<tr>
								<xsl:if test="position() mod 2 != 1">
									<xsl:attribute  name="class">high</xsl:attribute>

								</xsl:if>
								<td>
									<xsl:variable name="itemURL">
										<xsl:value-of select="sitemap:loc"/>
									</xsl:variable>
									<a href="{$itemURL}">
										<xsl:value-of select="sitemap:loc"/>
									</a>
								</td>

								<td>
									<xsl:value-of select="concat(sitemap:priority*100,'%')"/>
								</td>
							</tr>
						</xsl:for-each>
					</table>
				</div>
				<div id="footer">
					Программинг генерации карты сайта <a href="http://www.odblog.ru/">Дмитрий Охотников</a>, Шаблон XSLT <a href="http://www.arnebrachhold.de/">Arne Brachhold</a>. Все права сохранены.
				</div>

			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>