<?php
/** 
��� ������: ���������� ������ (��� �������, ������)
��� ������� ������ ���������� ����, �.�. ��� ��������.
*/

$module_name='texts';
$module_caption = '������';

$CONFIG=array(

	'module_name'=>$module_name,
	'module_caption'=>$module_caption,
	'fastcall'=>'/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'=>'1.1.0.0',		

	'tables'=>array(

		'items'=> array (

			'db_name' => $module_name,		
			'dialog'=> array('width'=>700,'height'=>550),
			'key_field'=>'id',
			'order_field' => '`title` DESC',
			'onpage' =>	9,

			'config'=>	array(

				'id' => array(
					'caption' => 'id',
					'type' => 'static',
							'in_list' => 1,
					
				),
				'title' => array(
							'caption' => '��������',
							'value' => '',
							'type' => 'string_root',
							'in_list' => 1,
							'filter' => 1,
							),
				'text' => array(
							'caption' => '�����',
							'value' => '',
							'height'=>260,
							'type' => 'wpage',
							'in_list' => 0,
							),
				),
			),
		),
);
?>