<?php
header('Content-type: text/html; charset=windows-1251');
global $CONFIG, $adminmenu, $DB, $Forms;
ob_clean();

global $CONFIG;
loadConfig();

$table_articles = $CONFIG['tables']['articles']['db_name'];
$table_items = $CONFIG['tables']['items']['db_name'];

$output = blockOnPage($CONFIG['module_name']);
$output_id = ($output>0 ? 'fast_table_'.$CONFIG['module_name'] : 'center');

//list($articles_isorder,$CONFIG['tables']['articles'])=moduleOrderField($CONFIG['tables']['articles']);
$articles_isorder=true;

//list($items_isorder,$CONFIG['tables']['items'])=moduleOrderField($CONFIG['tables']['items']);

checkModuleIntegrity();

switch($adminmenu->params[1]){	
	case 'config':
		ob_clean();
		$config = $_MODULES->by_dir($adminmenu->params[0]);
		global $_ZONES;
		$config['config'] = array();
		foreach ($_ZONES as $_zone){
			$config['config']['mod_'.$_zone['value']] = array(
				'caption' => $_zone['value'],
				'value' => (isset($config['output'][$_zone['value']]) ? $config['output'][$_zone['value']] : ''),
				'module'=>$adminmenu->params[0],
				'zone'=>$_zone['value'],
				'type' => 'explorer',
				);
		}
		
		$vars['_FORM_'] = $Forms->make($config['config']);
		$vars['mod'] = $adminmenu->params[0];
		
		die (template('module_config',$vars));
    break;	

	case 'fastview':
		$path_id = isset($adminmenu->params[2]) ? $adminmenu->params[2] : 0;
		$sel_page = isset($adminmenu->params[3]) ? $adminmenu->params[3] : 0;

		$tmp=$CONFIG['module_name'].'_articles_generateVars';
		$articles_vars = $tmp();

		$vars = array();
		$vars['CONFIG']		= $CONFIG; 
		$vars['DB']			= $DB; 
		$vars['Forms']		= $Forms; 
		$vars['path_id']	= $path_id;
		$vars['page']		= 1;
		$vars['articles']	= template('articles', $articles_vars);

		UseModule('ajax');
		$JsHttpRequest =& new JsHttpRequest("windows-1251");		
		$_RESULT = array('content' => template('fast',$vars));
	break;


	case 'swap_article':
		$path_id = isset($adminmenu->params[2]) ? $adminmenu->params[2] : 0;
		$action = (isset($adminmenu->params[3]) && $adminmenu->params[3] == 'up') ? 1:0;
		$article_id = isset($adminmenu->params[4]) ? $adminmenu->params[4] : 0;

		$tmp=$CONFIG['module_name'].'_setSwapItemsOrder';
		$tmp($CONFIG['tables']['articles']['db_name'], $article_id, $action);	
		
		$tmp=$CONFIG['module_name'].'_articles_generateVars';
		$articles_vars = $tmp();

		UseModule('ajax');
		$JsHttpRequest =& new JsHttpRequest("windows-1251");
		$_RESULT = array('content' => template('articles',$articles_vars));
	break;

	case 'toggle_vis_article':
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$article_id = (isset($adminmenu->params[3]))?(int)$adminmenu->params[3] : 0;

		$active = $DB->getOne('SELECT `visible` FROM '.PRFX.$CONFIG['tables']['articles']['db_name'].' WHERE id='.(int)$article_id);

		$DB->execute('UPDATE '.PRFX.$CONFIG['tables']['articles']['db_name'].' SET `visible`='.(int)!(boolean)$active.' WHERE id='.(int)$article_id);

		UseModule('ajax');
		$JsHttpRequest =& new JsHttpRequest("windows-1251");
		$_RESULT = array('content' => '<img border="0" src="/DESIGN/ADMIN/images/'.((int)!(boolean)$active==0 ? 'invis' : 'vis').'.gif" alt=""/>');
	break;

	case 'add_article':
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$article_id = (isset($adminmenu->params[3]))?(int)$adminmenu->params[3] : 0;
		$parent = (isset($adminmenu->params[4]))?(int)$adminmenu->params[4] : 0;
	
		$tmp=$CONFIG['module_name'].'_articles_generateConfigValues';
		$CONFIG = $tmp($article_id, $parent,$path_id);

		if (isset($_REQUEST['conf'])) {

			$tmp=$CONFIG['module_name'].'_articles_saveItem';
			$tmp();
			
			$tmp=$CONFIG['module_name'].'_articles_generateVars';
			$articles_vars = $tmp();
			
			UseModule('ajax');
			$JsHttpRequest =& new JsHttpRequest("windows-1251");
			$_RESULT = array('content' => template('articles',$articles_vars));
		} else {
			$vars['_FORM_'] = $Forms->make($CONFIG['tables']['articles']['config']);
			$vars['CONFIG'] = $CONFIG;

			$vars['id'] = (int)$article_id;
			$vars['path_id'] = (int)$path_id;
			$vars['output_id'] = $output_id;

			echo template('add_article',$vars);
		}
	break;

	case 'del_article':
		UseModule('ajax');
		$JsHttpRequest =& new JsHttpRequest("windows-1251");
		
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$parent = (isset($adminmenu->params[3]))?(int)$adminmenu->params[3] : 0;
		
		if ($parent != 0) {
			$tmp=$CONFIG['module_name'].'_deleteArticle';
			$tmp($parent);
		}
		
		$tmp=$CONFIG['module_name'].'_articles_generateVars';
		$articles_vars = $tmp();

		UseModule('ajax');
		$JsHttpRequest =& new JsHttpRequest("windows-1251");
		$_RESULT = array('content' => template('articles',$articles_vars));
	break;

####################################################################################################################################################
####################################################################################################################################################
########################################							�����						####################################################
####################################################################################################################################################
####################################################################################################################################################

	case 'filter':
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$sel_page = (isset($adminmenu->params[3]))?(int)$adminmenu->params[3] : 0;
		$art_id = (isset($adminmenu->params[4]))?(int)$adminmenu->params[4] : 0;

		$sel_page = !isset($sel_page) ? 1 : (int)$sel_page;
	
		$_SESSION['filters'][$CONFIG['tables']['items']['db_name']] = serialize($_REQUEST['filters']);

		auth_StoreSession();

		UseModule('ajax');
		$JsHttpRequest =& new JsHttpRequest("windows-1251");

		$tmp=$CONFIG['module_name'].'_showItems';
		$items_vars=$tmp($CONFIG,$art_id,$sel_page);

		$_RESULT = array('content' => template('items', $items_vars));
	break;


	case 'clear_filter':

		clearFilters($CONFIG['tables']['items']['db_name']);	

	case 'show_items':
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$sel_page = (isset($adminmenu->params[3]))?(int)$adminmenu->params[3] : 0;
		$parent = (isset($adminmenu->params[4]))?(int)$adminmenu->params[4] : 0;

		$sel_page = !isset($sel_page) ? 1 : (int)$sel_page;

		UseModule('ajax');
		$JsHttpRequest =& new JsHttpRequest("windows-1251");

		$tmp=$CONFIG['module_name'].'_showItems';
		$items_vars=$tmp($CONFIG,$parent,$sel_page);

		$_RESULT = array('content' => template('items', $items_vars));
	break;
	
	case 'swap_item':
		$action = (isset($adminmenu->params[2]) && $adminmenu->params[2] == 'up') ? 1:0;
		$item_id = isset($adminmenu->params[3]) ? $adminmenu->params[3] : 0;
		$sel_page = (isset($adminmenu->params[4]))?(int)$adminmenu->params[4] : 0;
		$parent = (isset($adminmenu->params[5]))?(int)$adminmenu->params[5] : 0;

		$tmp=$CONFIG['module_name'].'_setSwapItemsOrder';
		$tmp($CONFIG['tables']['items']['db_name'], $item_id, $action, false);	
		
		$tmp=$CONFIG['module_name'].'_showItems';
		$items_vars=$tmp($CONFIG,$parent,$sel_page);

		UseModule('ajax');
		$JsHttpRequest =& new JsHttpRequest("windows-1251");
		$_RESULT = array('content' => template('items',$items_vars));
	break;	

	case 'add_item':
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$parent = (isset($adminmenu->params[3]))?(int)$adminmenu->params[3] : 0;
		$sel_page = (isset($adminmenu->params[4]))?(int)$adminmenu->params[4] : 0;
		$new = (isset($adminmenu->params[5]))?(int)$adminmenu->params[5] : 0;

		$order=isset($CONFIG['tables']['items']['order_field']) ? $CONFIG['tables']['items']['order_field'] : '`order`';

		if($new>0){
			$list = $DB->getRow("SELECT * FROM `".PRFX.$CONFIG['tables']['items']['db_name']."` WHERE `id`=".$new);
			foreach($CONFIG['tables']['items']['config'] as $k => $v)
				if ($k!='order')$CONFIG['tables']['items']['config'][$k]['value'] = $list[$k];
		}

		$CONFIG['tables']['items']['config']['parent']['caption'] = '';
		$CONFIG['tables']['items']['config']['parent']['value'] = $parent;
		$CONFIG['tables']['items']['config']['parent']['type'] = 'hidden';
		
		
		if (isset($_REQUEST['conf'])) {
			
			$save_CONFIG['items']['config'] = $Forms->save($CONFIG['tables']['items']['config']);

			foreach($save_CONFIG['items']['config'] as $field => $info)
				$sql_[] = "`".$field."` = '".$DB->pre($info['value'])."'";
				
			if ($_REQUEST['id']>0)
				$sql = "UPDATE `".PRFX.$CONFIG['tables']['items']['db_name']."` SET ".implode(', ',$sql_)." WHERE `id` = ".(int)$_REQUEST['id'];
			else {
				
				if(!isset($CONFIG['tables']['items']['order_field'])){
				
					//-----------������� ���������� � ����������� �� ���� ��� ������� � �������
					if($CONFIG['tables']['articles']['add_new_on_top']){
						$order=$DB->GetOne("SELECT MIN(`order`) FROM `".PRFX.$CONFIG['tables']['items']['db_name']."` WHERE `parent`=".(int)$CONFIG['tables']['items']['config']['parent']['value']);
						$sql_[]="`order`=".((int)$order-1);
					} else {
						$order=$DB->GetOne("SELECT MAX(`order`) FROM `".PRFX.$CONFIG['tables']['items']['db_name']."` WHERE `parent`=".(int)$CONFIG['tables']['items']['config']['parent']['value']);
						$sql_[]="`order`=".((int)$order+1);
					}
				}

				$sql = "INSERT INTO `".PRFX.$CONFIG['tables']['items']['db_name']."` SET ".implode(', ',$sql_);
			}

			$DB->execute($sql);
			

			UseModule('ajax');
			$JsHttpRequest =& new JsHttpRequest("windows-1251");

			$tmp=$CONFIG['module_name'].'_showItems';
			$items_vars=$tmp($CONFIG,$parent,$sel_page);

			$_RESULT = array('content' => template('items', $items_vars));
		} else {
			$vars['CONFIG'] = $CONFIG;
			$vars['Forms'] = $Forms;
			$vars['DB'] = $DB;

			$vars['_FORM_'] = $Forms->make($CONFIG['tables']['items']['config']);
			$vars['id'] = (int)$new;
			$vars['path_id'] = $path_id;
			$vars['parent'] = $parent;
			$vars['page'] = $sel_page;

			$vars['output']=$output;


			echo template('add_item',$vars);
		}
	break;

	case 'delete_item':
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$art_id = (isset($adminmenu->params[3]))?(int)$adminmenu->params[3] : 0;
		$sel_page = (isset($adminmenu->params[4]))?(int)$adminmenu->params[4] : 0;
		$item_id = (isset($adminmenu->params[5]))?(int)$adminmenu->params[5] : 0;

		$tmp=$CONFIG['module_name'].'_deleteAbstractItem';
		$tmp('items',$item_id);

		UseModule('ajax');
		$JsHttpRequest =& new JsHttpRequest("windows-1251");

		/* CREATE VIEW */ 
		$filter = generateFilterWhereSQL();
		
		$tmp=$CONFIG['module_name'].'_showItems';
		$items_vars=$tmp($CONFIG,$art_id,$sel_page);
		$_RESULT = array('content' => template('items', $items_vars));
	break;
}  

die();	

?>		