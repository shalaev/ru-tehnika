<?php
/*
������������ ������� � � �������� � � ��������� ���������� parent
���������� ���������� - ���� visible (������ � ��������)
��� ��������� ���������� ���������� ����� ������� ��������� �����
*/


$module_name='catalog';
$module_caption = '�������';

$CONFIG = array (

	'module_name'  => $module_name,
	'module_caption' => $module_caption,
	'fastcall' => '/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'  => '2.0.0.0',

	'tables'=>array(
		/*
			�������� ������ "articles" � "items" �����������, ��� ����� ������������ � ��������� ��������
		*/
	
	
		//------------------------------�������-------------------------------------------------------
		'articles' => array (

			'db_name'=>$module_name.'_articles',
			'dialog' => array ('width'=>700,'height'=>500),
			'onpage'=>20,
			
			//------���� ����� �� ������ �� ���������� ����� �������
//			'order_field' => '`name`',
			
			//---------��������� ���� ��������� ����� ��������, ����� ��� ���� �� ����������--------
			'add_new_on_top' => false,
			
			//------����������� ���������� ������� �����������----------
			'max_levels' => 1,

			//------����, ������� ����� ���������� � ������
			'title_field' => 'name',
			'is_order' => true,

			'config' => array(

				'title_menu' => array(
						'caption' => '��������� ����',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 1,
				),
				
				'name' => array(
						'caption' => '���������',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 1,
				),

				'short_desc' => array(
						'caption' => '������� ��������',
						'value' => '',
						'type' => 'wysiwyg',
						'in_list'=> 0,
						'height' => 200,
				),
				
				'text' => array(
						'caption' => '��������',
						'value' => '',
						'type' => 'wysiwyg',
						'in_list'=> 0,
						'height' => 300,
				),
			
				'title_page' => array(
						'caption' => 'TITLE',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 1,
				),
				
				'desc_page' => array(
						'caption' => 'meta DESCRIPTION',
						'value' => '',
						'type' => 'string',
						'oblig'=> 0,
						'in_list' => 0,
				),
				
				'key_page' => array(
						'caption' => 'meta KEYWORDS',
						'value' => '',
						'type' => 'string',
						'oblig'=> 0,
						'in_list' => 0,
				),

			),
		),
		//------------------------------�������-------------------------------------------------------

		//------------------------------������-------------------------------------------------------
		'items' => array (

			'db_name'=>$module_name.'_items',
			'dialog' => array ('width'=>750,'height'=>700),
			'onpage'=>20,
			//------���� ����� �� ������ �� ���������� ����� �������
//			'order_field' => '`title`',

			//---------��������� ���� ��������� ����� ��������, ����� ��� ���� �� ����������--------
			'add_new_on_top' => true,

			'config' => array (
			
				'title_menu' => array(
						'caption' => '��������� � �������',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 1,
				),			

				'title' => array(
						'caption' => '���������',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 1,
				),
				'images' => array(
						'caption' => '�����������',
						'value' => '',
						'type' => 'file_group',
						'thumbs' => array(
							array(0,0),
							array(50,0),
							array(235,0),
						),
						'in_list'=>0,
						'filter'=>0,
				),
				
				'text_first' => array(
						'caption' => '������� ��������',
						'value' => '',
						'type' => 'wysiwyg',
						'in_list'=> 0,
						'height' => 300,
				),				
				
				'text' => array(
						'caption' => '��������',
						'value' => '',
						'type' => 'wysiwyg',
						'in_list'=> 0,
						'height' => 300,
				),
				'brand' => array(
						'caption' => '�����',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 0,
				),
				'power' => array(
						'caption' => '��������',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 1,
				),			
				'power_nom' => array(
						'caption' => '����������� ��������',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 0,
				),
				'power_max' => array(
						'caption' => '������������ ��������',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 0,
				),
				'engine_m' => array(
						'caption' => '����� ���������',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 1,
				),
				'engine' => array(
						'caption' => '���������',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 0,
				),
				'generator' => array(
						'caption' => '���������',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 0,
				),				
				'capacity' => array(
						'caption' => '������� ����',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 0,
				),
				'consuption' => array(
						'caption' => '������',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 0,
				),
				'size' => array(
						'caption' => '��������',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 0,
				),
				'weight' => array(
						'caption' => '���',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 0,
				),
				'controller' => array(
						'caption' => '����������',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 0,
				),				
				'text_small' => array(
						'caption' => '���. ���. ��������',
						'value' => '',
						'type' => 'memo',
						'height' => 50,
						'oblig'=> 1,
						'in_list' => 0,
				),
				'title_page' => array(
						'caption' => '��������� ��������',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 1,
				),
				
				'desc_page' => array(
						'caption' => 'meta DESCRIPTION',
						'value' => '',
						'type' => 'string',
						'oblig'=> 0,
						'in_list' => 0,
				),
				
				'key_page' => array(
						'caption' => 'meta KEYWORDS',
						'value' => '',
						'type' => 'string',
						'oblig'=> 0,
						'in_list' => 0,
				),

				'in_stock' => array(
						'caption' => '� �������',
						'value' => 1,
						'type' => 'boolean',
						'in_list'=>0,
						'filter'=> 0,
				),						
				
				'in_deal' => array(
						'caption' => '���������������',
						'value' => 1,
						'type' => 'boolean',
						'in_list'=>0,
						'filter'=> 0,
				),	

				'pdf' => array(
						'caption' => 'PDF-����',
						'value' => NULL,
						'type' => 'string',
						'in_list'=>0,
						'filter'=> 0,
				),								
				
			),
		),
		//------------------------------������-------------------------------------------------------
			

	),	 

);
?>
