<?php
/** 
��� ������: ���������� ������ (��� �������, ������)
��� ������� ������ ���������� ����, �.�. ��� ��������.
*/

$module_name='users_access';
$module_caption = '�����';

$CONFIG=array(

	'module_name'=>$module_name,
	'module_caption'=>$module_caption,
	'fastcall'=>'/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'=>'1.1.0.0',
	
	'show'=>false,

	'tables'=>array(

		'items'=> array (

			'db_name' => $module_name,		
			'dialog'=> array('width'=>700,'height'=>550),
			'key_field'=>'id',						
			'onpage' =>	9,	
			'log_title_field' => 'id', // ���� ��������� ��� ����������� �������
		
			'config'=>	array(

				'user_id' => array(
					'caption' => '������������',
					'value' => '',
					'type' => 'select',
					'in_list' => 1,
					'from'=> array(
						'table_name'=>PRFX.'users',
						'key_field' => 'user_id',
						'name_field'=>'username',
						'order'=>'username',
						),
					),
					
				'access_module' => array(
					'caption' => '������� ����������� � �������',
					'value' => '',
					'type' => 'group_checkbox',
					'in_list' => 1,
					'separator'=>'<br>',
					'from'=> array(
						'table_name'=>PRFX.'modules',
						'key_field' => 'module_id',
						'name_field'=>'module_caption',
						'order'=>'module_id',
						),
					),

				'acl_move' => array(
							'caption' => '����������',
							'value' => '',
							'type' => 'boolean',
							'in_list' => 1,
							),
				'acl_add' => array(
							'caption' => '���������',
							'value' => '',
							'type' => 'boolean',
							'in_list' => 1,
							),
				'acl_edit' => array(
							'caption' => '��������',
							'value' => '',
							'type' => 'boolean',
							'in_list' => 1,
							),
				'acl_del' => array(
							'caption' => '�������',
							'value' => '',
							'type' => 'boolean',
							'in_list' => 1,
							),

				'acl_link' => array(
							'caption' => '������� �� ������',
							'value' => '',
							'type' => 'boolean',
							'in_list' => 1,
							),

				'acl_vis' => array(
							'caption' => '����� ���������',
							'value' => '',
							'type' => 'boolean',
							'in_list' => 1,
							),
				),
			),
		),
);
?>