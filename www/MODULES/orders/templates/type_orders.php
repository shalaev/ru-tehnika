<?php
$d_width = (int)$CONFIG['tables']['orders_table']['dialog']['width'];
$d_height = (int)$CONFIG['tables']['orders_table']['dialog']['height'];
?>

<h3>���������� ��������</h3>

<table cellpadding="0" cellspacing="0" border="0" align="center" class="table" style="width: <?=$d_width-20;?>px; margin:0 auto">
	<THEAD>
		<tr>
			<?php
			foreach ($CONFIG['tables']['orders_table']['config'] as $field => $th)
				{
				if ($th['in_list']==1)
					{
					$width='';
					echo '<th '.$width.'><span style="white-space: nowrap">'.$th['caption'].'</span></th>';
					}
				}
			?>

			<th width="40">&nbsp;</th>
		</tr>
	</THEAD>
	<TBODY>
	<?
	foreach ($items as $item){

		if((int)$item['can_edit']==1)$ed_click="displayMessage('/".ROOT_PLACE."/".$CONFIG['module_name']."/edit_order/".(int)$item['type_order_id']."',".$d_width.",".$d_height.")";

		if((int)$item['can_del']==1)$del_click="if (confirm('�������?')) doLoad('','/".ROOT_PLACE."/".$CONFIG['module_name']."/del_order/".(int)$item['type_order_id']."/',currentDialogContent())";
		
	?>
		<tr style="cursor: pointer" onmouseover="this.style.backgroundColor='#FBFCE2';" onmouseout="this.style.backgroundColor='white'">
		<?php
		foreach ($CONFIG['tables']['orders_table']['config'] as $field => $td){
			if ($td['in_list']==1){
				echo '<td '.((int)$item['can_edit']==1?'ondblclick="'.$ed_click.'"':'').'>'.$item[$field].'</td>';
			}
		}
		?>
			<td width="40" align="center">
				<?if((int)$item['can_edit']==1)echo writeSmallEditButton($ed_click);?>
				<?if((int)$item['can_del']==1)echo writeSmallDeleteButton($del_click);?>
			</td>				
		</tr>
	<?}?>
	</TBODY>

</table>
<br>


<SCRIPT type="text/javascript">
dialogAddButtons(
	new Array(
		new Array("displayMessage('/<?=ROOT_PLACE;?>/<?=$CONFIG['module_name'];?>/add_order/', <?=$d_width;?>,<?=$d_height;?>)","��������")
	)
);
</SCRIPT>