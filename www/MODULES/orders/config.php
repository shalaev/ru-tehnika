<?php

$module_name='orders';
$module_caption = '������';

$CONFIG=array(

	'module_name'=>$module_name,
	'module_caption'=>$module_caption,
	'fastcall'=>'/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'=>'1.1.0.1',		

	'tables'=>array(

		'items'=> array (

			'db_name' => $module_name,
			'key_field'=>'order_id',
			'dialog'=> array('width'=>700, 'height'=>550),			  		
			'onpage' =>	20,	 
			
			'config'=>	array(
				'date' => array(
						'caption' => '����',
						'value' => '',
						'type' => 'calendar',
						'in_list'=>1,
						'filter'=>0,
				),			
			
				'name' => array(
							'caption' => '���',
							'value' => '',
							'height'=>100,
							'type' => 'string',
							'in_list' => 1,
							),

				'email' => array(
							'caption' => 'E-Mail',
							'value' => '',
							'height'=>100,
							'type' => 'string',
							'in_list' => 1,
							),
							
				'phone' => array(
							'caption' => '�������',
							'value' => '',
							'height'=>100,
							'type' => 'string',
							'in_list' => 1,
							),

				'message' => array(
							'caption' => '���������',
							'value' => '',
							'type' => 'wysiwyg',
							'in_list'=> 0,
							'height' => 300,
							),	

				'place' => array(
							'caption' => '���������������',
							'value' => '',
							'height'=>100,
							'type' => 'string',
							'in_list' => 1,
							),	

				'pages' => array(
							'caption' => '�� ��������',
							'value' => '',
							'height'=>100,
							'type' => 'string',
							'in_list' => 1,
							),								

				'orders_type' => array(
							'caption' => '��� ������',
							'value' => '',
							'from'=> array(
									'table_name'=>PRFX.'orders_type', 
									'key_field'=>'type_order_id',
									'name_field'=>'name',
									'where' => 'type_order_id NOT IN(1,3)',
									'order_by' => 'type_order_id'
							),
							'type' => 'select',
							'in_list' => 1,
							'filter'=>1,
							),

				),
			),


		'orders_table'=>array(

			'db_name' => $module_name.'_type',		
			'key_field'=>'type_order_id',
			'dialog'=> array('width'=>600,'height'=>350),
			'onpage' =>	23,				

			'config'=>	array(

				'name' => array(
							'caption' => '��� ������',
							'value' => '',
							'type' => 'string',
							'in_list' => 1,
							),

				),
			),
	
		),
);
?>