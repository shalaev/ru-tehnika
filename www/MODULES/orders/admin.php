<?php
header('Content-type: text/html; charset=windows-1251');
global $CONFIG, $adminmenu, $DB, $Forms;
ob_clean();

global $CONFIG;
loadConfig();	   

$table_name = $CONFIG['tables']['items']['db_name'];
list($isorder,$CONFIG['tables']['items'])=moduleOrderField($CONFIG['tables']['items']);
$key_field = $CONFIG['tables']['items']['key_field'];

list($output_id) = prepareLinkPath($CONFIG);

checkModuleIntegrity();

switch($adminmenu->params[1]){
	case 'config':
		ob_clean();

		$config = $_MODULES->by_dir($adminmenu->params[0]);

		global $_ZONES;

		$config['config'] = array();

		foreach ($_ZONES as $_zone)
			{
			$config['config']['mod_'.$_zone['value']] = array(
				'caption' => $_zone['value'],
				'value' => (isset($config['output'][$_zone['value']]) ? $config['output'][$_zone['value']] : ''),
				'module'=>$adminmenu->params[0],
				'zone'=>$_zone['value'],
				'type' => 'explorer',
				);		
			}	

		$vars['_FORM_'] = $Forms->make($config['config']);
		$vars['mod'] = $adminmenu->params[0];
		

		die (template('module_config',$vars));
	break;	



	case 'swap':
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$page = isset($adminmenu->params[3]) && is_numeric($adminmenu->params[3]) ? $adminmenu->params[3] : 0;
		$item_id= isset($adminmenu->params[4]) && is_numeric($adminmenu->params[4]) ? $adminmenu->params[4] : 0;
		$action = (isset($adminmenu->params[5]) && $adminmenu->params[5] == 'up') ? 1:0;

		/* ����������� ������� */
		$title = $DB->getOne("select `".(isset($CONFIG['tables']['items']['log_title_field']) ? $CONFIG['tables']['items']['log_title_field'] : 'title')."` from `".PRFX.$table_name."` where id=".$item_id);
		if ($action > 0) {					/* ����������� ������� - "����� �����" */
			if($log_file && isset($log_config['swap_up']) && $log_config['swap_up'] != '')
				$log_action = $log_config['swap_up'].' "'.$title.'"';
		}else{ 								/* ����������� ������� - "����� ����" */
			if($log_file && isset($log_config['swap_down']) && $log_config['swap_down'] != '')
				$log_action = $log_config['swap_down'].' "'.$title.'"';
		}

		setSwapItemsOrder($table_name, $item_id, $action);		
		$vars=generateVars('');

		UseModule('ajax');
		$JsHttpRequest =& new JsHttpRequest("windows-1251");		
		$_RESULT = array('content' => template('fast',$vars)); 
	break;



	case 'filter':
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$page = isset($adminmenu->params[3]) && is_numeric($adminmenu->params[3]) ? $adminmenu->params[3] : 1;
	
		$_SESSION['filters'][$table_name] = serialize($_REQUEST['filters']);
		$vars=generateVars('');

		UseModule('ajax');
		$JsHttpRequest =& new JsHttpRequest("windows-1251");		
		$_RESULT = array('content' => template('fast',$vars)); 
	break;


	case 'clear_filter':

		clearFilters();	


	case 'fastview': 
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$page = isset($adminmenu->params[3]) && is_numeric($adminmenu->params[3]) ? $adminmenu->params[3] : 1;

		$vars=generateVars();
		$vars['items']=$DB->GetAll("SELECT * FROM `".PRFX."orders` WHERE `order_id` NOT IN(1,2) ORDER BY `order_id` DESC");

		UseModule('ajax');
		$JsHttpRequest =& new JsHttpRequest("windows-1251");		
		$_RESULT = array('content' => template('fast',$vars)); 
		
	break;



	case 'reset_order':
		UseModule('ajax');
		$JsHttpRequest =& new JsHttpRequest("windows-1251");	

		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$art_id= isset($adminmenu->params[3]) && is_numeric($adminmenu->params[3]) ? $adminmenu->params[3] : 0;

		/* ����������� ������� - ����� ���������� */
		if($log_file && isset($log_config['reset_order']) && $log_config['reset_order'] != '')
			$log_action = $log_config['reset_order'];

		/* ACTION */
		resetSwapItemsOrder($table_name); 
		$vars=generateVars('');
		/* ACTION */

		$_RESULT = array('content' => template('fast',$vars));
	break;




	case 'add':
		{			
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$page = (isset($adminmenu->params[3]))?(int)$adminmenu->params[3] : 0;
		$new_item_id = (isset($adminmenu->params[4]))?(int)$adminmenu->params[4] : 0;

		/* ACTION */	  
		$CONFIG = generateConfigValues($new_item_id);

		if (isset($_REQUEST['conf']))
			{	
			UseModule('ajax');
			$JsHttpRequest =& new JsHttpRequest("windows-1251");

			saveItem();			
			list($isorder, $CONFIG['tables']['items'])=moduleOrderField($CONFIG['tables']['items']);			
			$vars=generateVars();

			//$_RESULT = array('content' => template('fast',$vars));
			echo template('fast',$vars);
			}
		else
			{
			$vars['CONFIG']		= $CONFIG;
			$vars['Forms']		= $Forms;

			$vars[$key_field]	= (int)$new_item_id;
			$vars['path_id']	= (int)$path_id;
			$vars['page']		= $page;	   
			$vars['output_id']	= $output_id;
			$vars['isorder']	= $isorder;

			$vars['_FORM_']		= $Forms->make($CONFIG['tables']['items']['config']);

			echo template('add',$vars);
			}
		/* ACTION */

		die();
		}
	break;	


	case 'delete':
		$path_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		$page= (isset($adminmenu->params[3]))?(int)$adminmenu->params[3] : 0;
		$id = (isset($adminmenu->params[4]))?(int)$adminmenu->params[4] : 0;
		
		/* ACTION */
		if($id>0){
			UseModule('ajax');
			$JsHttpRequest =& new JsHttpRequest("windows-1251");

			if($log_file && isset($log_config['delete']) && $log_config['delete'] != '') { /* ����������� ������� - "��������" */
				$title = $DB->getOne("select `".(isset($CONFIG['tables']['items']['log_title_field']) ? $CONFIG['tables']['items']['log_title_field'] : 'order_id')."` from `".PRFX.$table_name."` where order_id=".$id);
				$log_action = $log_config['delete'].' "'.$title.'"';
			}

			$DB->execute("DELETE FROM `".PRFX.$table_name."` WHERE `".$key_field."`=".$id);
			$vars=generateVars('');

			$_RESULT = array('content' => template('fast',$vars));
		}
		/* ACTION */		

		exit;		
	break;

//----------- ���������� �������� -------------
	case 'manage_orders':
			$groups = $DB->getAll("SELECT * FROM `".PRFX."orders_type` WHERE `type_order_id` NOT IN(1,3)");
			$vars['items'] = $groups;
			$vars['CONFIG'] = $CONFIG;
			$vars['DB'] = $DB;
			$vars['Forms'] = $Forms;

			echo template('type_orders',$vars);		
	break;
	
	case 'add_order':
		if (!isset($_REQUEST['conf'])){
			// ������� ����� ����������
			$vars['_FORM_'] = $Forms->make($CONFIG['tables']['orders_table']['config']);
			
			$vars['CONFIG'] = $CONFIG;
			
			echo template('order_add',$vars);
		} else {
			// ��������� ������ � ��
			UseModule('ajax');
			$JsHttpRequest =& new JsHttpRequest("windows-1251");
			
			$CONFIG['tables']['orders_table']['config'] = $Forms->save($CONFIG['tables']['orders_table']['config']);

			$sql_ = array();			
			foreach($CONFIG['tables']['orders_table']['config'] as $field => $info)
				$sql_[] = "`".$field."` = '".$DB->pre($info['value'])."'";
			
			$sql = "INSERT INTO `".PRFX.$CONFIG['tables']['orders_table']['db_name']."` SET ".implode(', ',$sql_);

			$DB->execute($sql);
			
			// ��������� ���������� � ���� � �������
			$groups = $DB->getAll("SELECT * FROM `".PRFX."orders_type` ");
			$vars['items'] = $groups;
			$vars['CONFIG'] = $CONFIG;
			
			$_RESULT = array('content' => template('type_orders',$vars));
		}
	break;	
	
	case 'edit_order':
			if (!isset($_REQUEST['conf'])){
				// ������� ����� ��������������
				$editing_group_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
				
				if ($editing_group_id <= 0) die ("�� ������� ������ ��� ��������������.");
				
				$list = $DB->getRow("SELECT * FROM `".PRFX.$CONFIG['tables']['orders_table']['db_name']."` WHERE `".$CONFIG['tables']['orders_table']['key_field']."` = ".$editing_group_id);

				foreach($CONFIG['tables']['orders_table']['config'] as $k => $v)
					$CONFIG['tables']['orders_table']['config'][$k]['value'] = $list[$k];
				
				$vars['_FORM_'] = $Forms->make($CONFIG['tables']['orders_table']['config']);
				
				$vars['CONFIG'] = $CONFIG;
				$vars['editing_group_id'] = $editing_group_id;
				
				echo template('order_edit',$vars);
			}
			else {
				// ��������� ������ � ��
				UseModule('ajax');
				$JsHttpRequest =& new JsHttpRequest("windows-1251");
				
				$editing_group_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
				
				$CONFIG['tables']['orders_table']['config'] = $Forms->save($CONFIG['tables']['orders_table']['config']);

				$sql_ = array();
				foreach($CONFIG['tables']['orders_table']['config'] as $field => $info)
					$sql_[] = "`".$field."` = '".$DB->pre($info['value'])."'";
				
				$sql = "UPDATE `".PRFX."orders_type` SET ".implode(', ',$sql_)." WHERE `".$CONFIG['tables']['orders_table']['key_field']."` = ".$editing_group_id." LIMIT 1";
			
				$DB->execute($sql);
				
				// ��������� ���������� � ���� � �������
				$groups = $DB->getAll("SELECT * FROM `".PRFX."orders_type` ");
				$vars['items'] = $groups;
				$vars['CONFIG'] = $CONFIG;
				
				$_RESULT = array('content' => template('type_orders',$vars));
			}
	break;
	case 'del_order':
		// ������� ������ �� ��
		UseModule('ajax');
		$JsHttpRequest =& new JsHttpRequest("windows-1251");
		
		$deleting_group_id = (isset($adminmenu->params[2]))?(int)$adminmenu->params[2] : 0;
		
		if ($deleting_group_id <= 0) 
			{
			$_RESULT = array('content' => "�� ������� ������� ������. ");	
			}
		else 
			{
			$sql = "DELETE FROM `".PRFX.$CONFIG['tables']['orders_table']['db_name']."` WHERE `type_order_id`=".$deleting_group_id." LIMIT 1";
			$DB->execute($sql);
			
			// ��������� ����
			$groups = $DB->getAll("SELECT * FROM `".PRFX."orders_type` ");
			$vars['items'] = $groups;
			$vars['CONFIG'] = $CONFIG;
			
			$_RESULT = array('content' => template('type_orders',$vars));
			}
	break;
	//----------/-���������� �������� -------------
	
	}						

die();				   
?>