<?php
/** 
��� ������: ���������� ������ (��� �������, ������)
��� ������� ������ ���������� ����, �.�. ��� ��������.
*/

$module_name='titles';
$module_caption = '��������� (titles)';

$CONFIG=array(

	'module_name'=>$module_name,
	'module_caption'=>$module_caption,
	'fastcall'=>'/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'=>'1.1.0.0',		

	'tables'=>array(

		'items'=> array (

			'db_name' => $module_name,		
			'dialog'=> array('width'=>700,'height'=>550),
			'key_field'=>'id',
			'order_field' => '`sort`',
			'onpage' =>	9,

			'config'=>	array(

				'url' => array(
							'caption' => '����� URL� (% - ����� ������)',
							'value' => '',
							'type' => 'string',
							'in_list' => 1,
							'filter' => 0,
							),
				'type_insert' => array(
							'caption' => '����� �������',
							'type' => 'select',
							'values' => array(
								0 => '������',
								1 => '�� ���������',
								2 => '����� ���������',
								3 => '�� ����������',
								4 => '����� ����������',
							),
							'in_list' => 1,
							
				),
				'text' => array(
							'caption' => '�����',
							'value' => '',
							'height'=>70,
							'type' => 'memo',
							'filter' => 0,
							'in_list' => 1,
							),
				'sort' => array(
					'caption' => '����',
					'type' => 'string',
					'value' => 0,
					'in_list' => 1,
				),
				'active' => array(
					'caption' => '��������',
					'type' => 'boolean',
					'value' => 1,
					'in_list' => 1,
					'filter' => 0,
				),
				),
			),
		),
);
?>