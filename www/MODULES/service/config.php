<?php
/** 
��� ������: ���������� ������ (��� �������, ������)
��� ������� ������ ���������� ����, �.�. ��� ��������.
*/

$module_name='service';
$module_caption = '������';

$CONFIG=array(

	'module_name'=>$module_name,
	'module_caption'=>$module_caption,
	'fastcall'=>'/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'=>'1.1.0.0',


	'tables'=>array( 

		'items'=>array(	  

			'db_name' => $module_name,				   
			'dialog'=> array('width'=>800,'height'=>750),
			'key_field'=>'id',						   
			'onpage' =>	20,	

			'config'=>	array(	
				'title' => array(
						'caption' => '� ������',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 1,
				),
				'active' => array(
						'caption' => '�������',
						'value' => 1,
						'type' => 'boolean',
						'in_list'=>1,
						'filter'=>0,
				),
			),
		),
	)
);
?>
