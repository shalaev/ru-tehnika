<?php
/** 
��� ������: ���������� ������ (��� �������, ������)
��� ������� ������ ���������� ����, �.�. ��� ��������.
*/

$module_name='videoblog';
$module_caption = '���������';

$CONFIG=array(

	'module_name'=>$module_name,
	'module_caption'=>$module_caption,
	'fastcall'=>'/'.ROOT_PLACE.'/'.$module_name.'/fastview/',
	'version'=>'1.1.0.0',


	'tables'=>array( 

		'items'=>array(	  

			'db_name' => $module_name,				   
			'dialog'=> array('width'=>800,'height'=>750),
			'key_field'=>'id',						   
			'onpage' =>	20,	

			'config'=>	array(
				'date' => array(
						'caption' => '����',
						'value' => '',
						'type' => 'calendar',
						'in_list'=>1,
						'filter'=>0,
				),			
			
				'title' => array(
						'caption' => '���������',
						'value' => '',
						'type' => 'string',
						'oblig'=> 1,
						'in_list' => 1,
				),
				'announce' => array(
						'caption' => '�����',
						'value' => '',
						'type' => 'wysiwyg',
						'in_list'=> 0,
						'height' => 300,
				),

				'active' => array(
						'caption' => '�������',
						'value' => 1,
						'type' => 'boolean',
						'in_list'=>1,
						'filter'=>0,
				),
				
			),
		),
	)
);
?>
