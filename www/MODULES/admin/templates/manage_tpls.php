<?php
$all = $DB->getAll('SELECT * FROM `'.PRFX.'tpl` ORDER BY id');
if (sizeof($all))
	{
	echo '<Table class="table">';
	echo '
		<tr>
			<th width="32">ID</th>
			<th>Название</th>
			<th width="48">Действия</th>
		</tr>
	';

	foreach ($all as $num => $item)
		{
		$ed_click='displayMessage(\'/'.ROOT_PLACE.'/admin/manage_tpls/edit/'.$item['id'].'/\',400,300);';
		$del_click='doLoad(\'\', \'/'.ROOT_PLACE.'/admin/manage_tpls/delete/'.$item['id'].'/\', \'center\')';

		echo '
			<tr>
				<td align="center">'.$item['id'].'</td>
				<td style="padding: 5px;">'.$item['caption'].'</td>
				<td align="center">
					'.writeSmallEditButton($ed_click).'	
					'.writeSmallDeleteButton($del_click).'			
				</td>
			</tr>
		';
		}
	
	echo '</table>';
	}
?>

