<?php

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// YML                                                       //
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
// ����� �������: ������� ���������                          //
// e-mail: od@od7.ru                                         //
// http://ohotnikov.pro                                      //
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //

# ������ ��������� ���� ������
define("DB_HOST", "localhost"); // ����
define("DB_NAME", "u10292"); // ��� ��
define("DB_USER", "u10292"); // ��� �����-��
define("DB_PASS", "vodag1potah"); // ������ � ��

# ����������� � ����
if (!($conn = mysql_connect(DB_HOST, DB_USER, DB_PASS)))
    exit(); // ��� ��������� ����������
mysql_select_db(DB_NAME, $conn); // ����� ��
mysql_query("SET NAMES 'windows-1251'"); // ���������� windows-1251

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

header("Content-Type: text/xml; charset=windows-1251");

echo '<?xml version="1.0" encoding="windows-1251"?>';
echo '<!DOCTYPE yml_catalog SYSTEM "shops.dtd">';
echo '<yml_catalog date="'.date('Y-m-d H:i').'">';
echo '<shop>';
echo '<name>��-�������</name>';
echo '<company>��-�������: ������������ � ������� ��������� ��������������</company>';
echo '<url>http://www.ru-tehnika.ru/</url>';
echo '<currencies>';
echo '<currency id="RUR" rate="1"/>';
echo '</currencies>';
echo '<categories>';
echo '<category id="1">��������� ��������������</category>';
echo '<category id="2" parentId="1">�������������� ����� ��</category>';
echo '</categories>';
echo '<offers>';

// �������� �����������

echo '<offer id="65" type="vendor.model" available="true">';
echo '<url>http://www.ru-tehnika.ru/catalog/6/65/</url>';
echo '<price>594000</price>';
echo '<currencyId>RUR</currencyId>';
echo '<categoryId>2</categoryId>';
echo '<picture>http://www.ru-tehnika.ru/UPLOAD/2011/07/01/ad100.png</picture>';
echo '<delivery>true</delivery>';
echo '<typePrefix>����� ��</typePrefix>';
echo '<vendor>��-�������</vendor>';
echo '<model>��-100-T400-1P � ���</model>';
echo '<description>��������� �������������� ��-100-�400 � ���������� ���-238, �������� ������������: ��������, � ����������, �� �����, �� ����� ��� �� �� ���������.</description>';
echo '<manufacturer_warranty>true</manufacturer_warranty>';
echo '<country_of_origin>������</country_of_origin>';
echo '<param name="�������� ����������� / ������������">100��� / 110���</param>';
echo '<param name="���������">���-238</param>';
echo '<param name="��������" unit="��">2404�1110�1472</param>';
echo '<param name="��������� ���" unit="�">250</param>';
echo '<param name="������ ������� ��� 75% ��������" unit="�/�">22</param>';
echo '</offer>';

echo '<offer id="442" type="vendor.model" available="true">';
echo '<url>http://www.ru-tehnika.ru/catalog/6/442/</url>';
echo '<price>396000</price>';
echo '<currencyId>RUR</currencyId>';
echo '<categoryId>2</categoryId>';
echo '<picture>http://www.ru-tehnika.ru/UPLOAD/2011/11/24/ad100-ed.png</picture>';
echo '<delivery>true</delivery>';
echo '<typePrefix>����� ��</typePrefix>';
echo '<vendor>��-�������</vendor>';
echo '<model>��-100-T400-1P � Ricardo</model>';
echo '<description>��������� �������������� ��-100-�400 � ���������� Ricardo-WR6105IZLD, �������� ������������: ��������, � ����������, �� �����, �� ����� ��� �� �� ���������.</description>';
echo '<manufacturer_warranty>true</manufacturer_warranty>';
echo '<country_of_origin>�����</country_of_origin>';
echo '<param name="�������� ����������� / ������������">100��� / 110���</param>';
echo '<param name="���������">WR6105IZLD</param>';
echo '<param name="��������" unit="��">2404�1110�1472</param>';
echo '<param name="��������� ���" unit="�">250</param>';
echo '<param name="������ ������� ��� 75% ��������" unit="�/�">22</param>';
echo '</offer>';

echo '<offer id="443" type="vendor.model" available="true">';
echo '<url>http://www.ru-tehnika.ru/catalog/6/443/</url>';
echo '<price>494000</price>';
echo '<currencyId>RUR</currencyId>';
echo '<categoryId>2</categoryId>';
echo '<picture>http://www.ru-tehnika.ru/UPLOAD/2011/11/24/ad100-deutz.png</picture>';
echo '<delivery>true</delivery>';
echo '<typePrefix>����� ��</typePrefix>';
echo '<vendor>��-�������</vendor>';
echo '<model>��-100-T400-1P � Deutz</model>';
echo '<description>��������� �������������� ��-100-�400 � ���������� Deutz, �������� ������������: ��������, � ����������, �� �����, �� ����� ��� �� �� ���������.</description>';
echo '<manufacturer_warranty>true</manufacturer_warranty>';
echo '<country_of_origin>�����</country_of_origin>';
echo '<param name="�������� ����������� / ������������">100��� / 110���</param>';
echo '<param name="���������">DEUTZ TBD226B-6D(M)</param>';
echo '<param name="��������" unit="��">2404�1110�1472</param>';
echo '<param name="��������� ���" unit="�">250</param>';
echo '<param name="������ ������� ��� 75% ��������" unit="�/�">22</param>';
echo '</offer>';

echo '<offer id="444" type="vendor.model" available="true">';
echo '<url>http://www.ru-tehnika.ru/catalog/6/444/</url>';
echo '<price>590000</price>';
echo '<currencyId>RUR</currencyId>';
echo '<categoryId>2</categoryId>';
echo '<picture>http://www.ru-tehnika.ru/UPLOAD/2013/06/15/apd-145c_3.jpg</picture>';
echo '<delivery>true</delivery>';
echo '<typePrefix>����� ��</typePrefix>';
echo '<vendor>��-�������</vendor>';
echo '<model>��-100-T400-1P � Cummins</model>';
echo '<description>��������� �������������� ��-100-�400 � ���������� Cummins, �������� ������������: ��������, � ����������, �� �����, �� ����� ��� �� �� ���������.</description>';
echo '<manufacturer_warranty>true</manufacturer_warranty>';
echo '<country_of_origin>��������������</country_of_origin>';
echo '<param name="�������� ����������� / ������������">100��� / 110���</param>';
echo '<param name="���������">Cummins 6BTAA5.9G2</param>';
echo '<param name="��������" unit="��">2404�1110�1472</param>';
echo '<param name="��������� ���" unit="�">250</param>';
echo '<param name="������ ������� ��� 75% ��������" unit="�/�">22</param>';
echo '</offer>';

echo '</offers>';
echo '</shop>';
echo '</yml_catalog>';
?>