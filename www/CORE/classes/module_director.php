<?php
/**
* ����� ������ � �������� � �������
*/
class ModuleDirector {

    var $info= array ();

    /**
    *  ��������������� ������ �������� �������
    *  @see by_dir()
    *  @access public
    *  @var array
    */
    var $dirs= array ();

    /**
    *  ��������� ������.
    *  �� �������� �������������. 
    *  @access private
    */
    var $sys_modules= array ('admin','ajax');

    /**
    *  ���������� ��������� �������� ����� ������ �� ��������� 
    *  (������������ ��� ���������� ������ Invictum)
    *  @access public
    *  @return boolean �������� ���������
    *  @param string $module_name �������� ������
    */
    function isSystem($module_name) {
        return in_array($module_name, $this->sys_modules);
    }

    /**
    *  ����������� ������
    *  @access private
    *  @param string $module_name �������� ������
    *  @param const $file __FILE__
    *  @param const $line __LINE__
    *  @param version $ver_from �� ������, ������������
    *  @param version $ver_to �� ������ , ������������
    */
    function UseModule($module_name, $file, $line, $ver_from= null, $ver_to= null) {
		
		if (!isModule($module_name))
            $this->getNotInstalledModules(); 

        $module_path= MODULES_DIR.$module_name;
        $client_file= EndSlash($module_path).'client.php';	 
        $config_file= EndSlash($module_path).'config.php';		

        if ($this->already_included($config_file))
            return;

        if ($this->already_included($client_file))
            return;			 

        if (is_file($config_file)) {global $CONFIG; require_once ($config_file);} 
		else {echo '<div>����������� <B>config.php</B> � ������ <B>'.$module_name.'</B></div>';exit;}

		if (is_file($client_file)) {require_once ($client_file);} 
		else {echo '<div>����������� <B>client.php</B> � ������ <B>'.$module_name.'</B></div>';exit;}
    }

    /**
    *  ���������, ��������� �� ��� ������������� ����
    *  @access public
    *  @return boolean ��� ���������
    *  @param string $file FilePath
    */
    function already_included($file) {

        $included= get_included_files(); 
        if (strpos($included[0], '\\') !== false) {
            if (in_array(str_replace('/', '\\', $file), $included))
                return true;
        }
        else
            if (in_array($file, $included))
                return true;

        return false;

    }

    /**
    *  ������ � {@link $info � $dir} ��������� ��������� �������������� �������.
    *  @access private
    *  @staticvar boolean ������ ��� �������� �� ������ ����� �������
    */
    function getNotInstalledModules() {
        static $already_call= false;

        if ($already_call)
            return false;

        //$already_call= true;

        $dirs= GetDirsFromDir(MODULES_DIR);
		
        foreach ($dirs as $module_dir) {
            if (!isset ($this->dirs[$module_dir])) {
                $module= $this->readConfig($module_dir);
                
                $this->dirs[$module_dir]= $module['module_name'];
                $this->info[$module['module_name']]= $module;
                
            }
        }

    }

    /**
    *  ��������� ������ ������ �� ����� config.php, �������������� �������� ���������.
    *  ���� ������ ����������, �� ��������� ������������ ������ �����������.
    *  @access private
    *  @param string $module_dir ������� ������
    *  @return false|array ������������ � �������� ������
    */
    function readConfig($module_dir) {
        static $cache= array ();

        $module_dir= (string) $module_dir;

        if (isset ($cache[$module_dir]))
            return $cache[$module_dir];

        $config_file= EndSlash(MODULES_DIR.$module_dir).'config.php';
        if (is_file($config_file))
            require ($config_file);
        else
            return false;

        $CONFIG= isset ($CONFIG) ? (array) $CONFIG : array ();

        $module= & $cache[$module_dir];
        $module['module_id']= 0;
        $module['module_name']= isset ($CONFIG['module_name']) ? (string) $CONFIG['module_name'] : 'noname';
        $module['version']= isset ($CONFIG['version']) ? (string) $CONFIG['version'] : '0.0.0.1';
        $module['module_caption']= isset ($CONFIG['module_caption']) ? (string) $CONFIG['module_caption'] : '����������';
        $module['fastcall']= isset ($CONFIG['fastcall']) ? (string) $CONFIG['fastcall'] : '';
        $module['config']= isset ($CONFIG['config']) ? (array) $CONFIG['config'] : array ();
        $module['module_dir']= $module_dir;
		
        if (isset ($this->dirs[$module_dir]) && $this->info[$this->dirs[$module_dir]]['installed']) {
        	
            $current= & $this->info[$this->dirs[$module_dir]];
            foreach ($module['config'] as $key => $data)
                if (isset ($current['config'][$key]['value']))
                    $module['config'][$key]['value']= $current['config'][$key]['value'];

            $module['installed']= 1;
            $module['module_id']= $current['module_id'];
        }
        else
            $module['installed']= $module['module_id']= 0;


        unset ($config_file, $CONFIG, $module, $key, $data, $current);
        return $cache[$module_dir];
    }



    /**
    *  ������� ��� ���������� � ���� ������������� �������
    *  ���������� ���������� ���������� � ������������ � �������� � ������������� � {@link $info} � {@link $dirs}
    *  @access private
    *  @return void
    */
    function getModulesInfo() {
        global $DB;
        $this->info = array();
        $this->dirs = array();
        $list= $DB->getAll("SELECT * FROM `".PRFX."modules` ORDER BY `module_name` ", false);

        foreach ($list as $module) {
            $module['installed']= 1;
            $this->AddConfigModule($module);
        }

        unset ($list, $result, $module);

    }

    /**
    *  ���������� ���������� � ��������� ������ � ���������� ����.
    *  �������������� ��������������� ���������� {@link $info} � {@link $dir}
    *  @access private
    *  @param array $module StructureModuleCompact
    */
    function AddConfigModule($module) {

        if (!isset ($module['version']) || trim($module['version']) == '')
            $module['version']= '0.0.0.1';

        if (!isset ($module['module_caption']) || !trim($module['module_caption']))
            $module['module_caption']= '���������� ������';

        if (!isset ($module['module_name']) || !trim($module['module_name']))
            $module['module_name']= 'noname';

        $module['module_id']= isset ($module['module_id']) ? (integer) $module['module_id'] : 0;
        $module['installed']= isset ($module['installed']) ? (integer) $module['installed'] : 0;

        $arrays= array ('config','output');

        foreach ($arrays as $field) {

            if (isset ($module[$field]) && !is_array($module[$field]))
                $module[$field]= @ unserialize($module[$field]);

            if (!isset ($module[$field]))
                $module[$field]= array ();

            $module[$field]= (array) $module[$field];
        }

        foreach ($module['config'] as $key => $field)
            if (!isset ($field['value']))
                $module['config'][$key]['value']= false;

        /** ��������� � ���������� ����, ����������� �� ������ ������� */
        $old_module= $this->by_dir($module['module_dir']);
        if ($old_module === false)
            $old_module= array ();

        $module= array_merge($old_module, $module);

        $this->info[$module['module_name']]= $module;
        $this->dirs[$module['module_dir']]= $module['module_name'];

    }

    /**
    *  ���������� ��������� ������ �� ���������� ��� ������������
    *  @access public
    *  @param string $module_dir ������� ������ 
    *  @param  string $field ������� ���������� ����
    *  @return mixed ModuleStructure
    */
    function by_dir($module_dir, $field= null) {

        if (!isset ($this->dirs[$module_dir]))
            return false;

        return $this->by_name($this->dirs[$module_dir], $field);

    }

    /**
    *  ���������� ��������� ������ �� ����� ������
    *  @access public
    *  @param string $module_name ��� ������ 
    *  @param string $field ������� ���������� ����
    *  @return boolean|array ModuleStructure
    */
    function by_name($module_name, $field= null) {

        if (!isset ($this->info[$module_name]))
            return false;

        if (empty ($field))
            return $this->info[$module_name];

        if (!isset ($this->info[$module_name][$field]))
            return false;

        return $this->info[$module_name][$field];

    }


    /**
    *  ���������� ������ (��������� ���� config.php ������) � ������ ����������� � ��
    *  @access private
    *  @return boolean Operation OK
    *  @param string $module_dir ���������� ������
    */
    function InstallModule($module_dir) {
        global $DB;

        $file= EndSlash(MODULES_DIR.$module_dir).'client.php';

        if (is_file($file) && !$this->already_included($file))
            include $file;

        $module= $this->readConfig($module_dir);
        
        if ($module === false)
            die('��� ������ ��� ����������� ���� <B>config.php</B>: '.$module['module_name'].' | '.$module_dir.'');


        $sql= array ();
        $sql[]= "`module_name` = '".$DB->pre($module['module_name'])."'";
        $sql[]= "`module_caption` = '".$DB->pre($module['module_caption'])."'";
        $sql[]= "`version` = '".$DB->pre($module['version'])."'";
        $sql[]= "`module_dir` = '".$DB->pre($module['module_dir'])."'";
        $sql[]= "`config` = '".$DB->pre(serialize($module['config']))."'";
        $sql[]= "`fastcall` = '".$DB->pre($module['fastcall'])."'";

        if ($module['module_id'] == 0) {
            $DB->execute("INSERT INTO `".PRFX."modules` SET ".implode(",\r\n", $sql));
        }
        else {
            $DB->execute("UPDATE `".PRFX."modules` SET ".implode(",\r\n", $sql)." WHERE `module_id` = ".$module['module_id']);
        }


		if (is_file($install_file= EndSlash(MODULES_DIR.$module['module_dir']).'install.php')){
            include ($install_file);
		}
    }

    /**
    *  �������������� ������
    *  @access private
    *  @param string $module_dir ���������� ������
    *  @return void
    */
    function UnInstallModule($module_dir) {
        global $DB, $_MODULES;

        $module= $this->by_dir($module_dir);

        if ($module === false || !$module['installed'])
            die('�������� ������ �� ������� ����������������: ������ '.$module['module_name'].' �� ������ ��� �� ����������.<BR>'.$module_dir.'');

		$UnInstall_file= EndSlash(MODULES_DIR.$module['module_dir']).'uninstall.php';
        if (is_file($UnInstall_file))
            require_once ($UnInstall_file);

        $DB->execute("DELETE FROM `".PRFX."modules` WHERE `module_id` = '".$module['module_id']."'");

    }
}
?>