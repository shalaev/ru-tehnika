<?php
/**
** author: Roma Kilanov & Misha Lysov
** date:	23.07.07

** Example:

	$mail = new SendMail();
	$mail->init();
	$mail->setEncoding("windows-1251");
	$mail->setEncType("base64");
	$mail->setTo("mail@mail.ru");
	$mail->setSubject("���������");
	$mail->setMessage("�����");
	$mail->setFrom("to@mail.ru", "���� �������");				// ������ �� ������������ ��������
	$mail->setFiles(array('/path/to/file1','/path/to/file2'));	// ������ ���� �� �����
	$mail->send();
**/

class SendMail
	{
	var $boundary;
	var $toMail;
	var $subject;
	var $message;
	var $submessage;
	var $from;

	function SendMail()
		{
		
		}

	function init()
		{
		$this->boundary = md5(time());
		$this->toMail='';
		$this->subject='';
		$this->message='';
		$this->submessage='';
		$this->from='';
		$this->enctype='quoted-printable';
		$this->encoding='windows-1251';		
		}

	function setTo($to)
		{
		$this->toMail=$to;
		}
  
	function setSubject($subj)
		{
		$this->subject=$this->encodeHeaders(($subj!="" ? $subj : '����� ���������'));
		}

	function setMessage($mess)
		{
		  if ($this->enctype == 'base64'){
		    $this->message=$this->base_64_encode(($mess!="" ? $mess : '����� ���������'));
		  } else {
		  	$this->message=$this->quoted_printable_encode(($mess!="" ? $mess : '����� ���������'));
		  }		    
		}
	
	function setEncType($enctype){
	  $this->enctype = $enctype;	  
	}	
  
	function setEncoding($encoding){
	  $this->encoding = $encoding;	  
	}

  function setFrom($fromMail, $fromName="")
		{
		if ($fromName!='')     $this->from=$this->encodeHeaders($fromName).' <'.$fromMail.'>';
        else                    $this->from=$fromMail;
	    }

	function setFiles($files)
		{
		$files=((is_array($files) && sizeof($files)>0) ? $files : array());
		$this->submessage=((is_array($files) && sizeof($files)>0) ? $this->appendFiles($this->boundary, $files) : '');
		}

	function send()
		{
		$headers  = 'MIME-Version: 1.0' . "\n";
		$headers .= "From: ".$this->from."\n";
		$headers .= "Content-Type: multipart/mixed;\n";
		$headers .= "  boundary=\"".$this->boundary."\"\n";

		$message = "--".$this->boundary."\n".
		"Content-type: text/html; charset=".$this->encoding."\n".
		"Content-Transfer-Encoding: ".$this->enctype."\n\n".
		$this->message.$this->submessage."\n--".$this->boundary."--\n\n";

		$this->toMail=str_replace(";",",",$this->toMail);

		return mail($this->toMail, $this->subject, $message, $headers);
		}

	function quoted_printable_encode_character ( $matches ) 
		{
		$character = $matches[0];
		return sprintf ( '=%02x', ord ( $character ) );
		}

		
	/**
	 * ����������� ���� ������ � base64
	 *
	 * @param string $string
	 * @return string
	 */
	function base_64_encode ( $string ) 
		{		
		 $string = base64_encode($string);
		 $newline = "\r\n";
		 $string = preg_replace ( '/(.{76})/', '$1'.$newline, $string);		 
		 return $string;		 		 
		 
		}

		
			 
		
	/**
	 * ����������� ���� ������ � quoted-printable
	 *
	 * @param string $string
	 * @return string
	 */
	function quoted_printable_encode ( $string ) 
		{
		$string = preg_replace_callback ('/[^\x21-\x3C\x3E-\x7E\x09\x20]/', create_function('$matches', 'return SendMail::quoted_printable_encode_character($matches[0]);'), $string );
		$newline = "=\r\n";
		$string = preg_replace ( '/(.{73}[^=]{0,3})/', '$1'.$newline, $string);
		return $string;
		}

	/**
	 * ����������� ���������� � base64
	 *
	 * @param string $header
	 * @return string
	 */
	function encodeHeaders($header)
		{
		return "=?".$this->encoding."?B?".base64_encode($header)."?=";
		}

	/**
	 * ������� ���������� ������ � ��������������� �������
	 *
	 * @param string $boundary multipart delimiter
	 * @param array $files full path to all files attached
	 * @return unknown
	 */
	function appendFiles($boundary, $files)
		{
		$string = '';
		foreach ($files as $file)
			{
			$filename = explode("/", $file);
			$filename = $this->encodeHeaders($filename[count($filename)-1]);
			$file_contents = base64_encode(file_get_contents($file));
			$file_contents = preg_replace ( '/(.{72})/', '$1'."\n", $file_contents);

			$string .= "\n\n--$boundary\n";
			$string .= "Content-Type: text/plain; name=\"$filename\"\n";
			$string .= "Content-transfer-encoding: base64\n";
			$string .= "Content-Disposition: attachment; filename=\"$filename\"\n";
			$string .= "\n\n";
			$string .= $file_contents;
		  }

		return $string;
		}
	}
?>