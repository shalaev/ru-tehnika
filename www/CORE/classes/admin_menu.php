<?php


/**
* ����� ���������� ����������������� ����.
* ���� ������������ �� ������ ���������
* ���������������� ��������, ������.
*
* @package Core

*/

/**
*  ����� ������������ ��� ���������� ����������������� ���� � ������ menu.php.
*  ����� �������� ������� ������ � ���������������� �������.
*  ��� ������ ������ ������������ {@link $modules}.
*  @access public
*/
class Admin_Menu {

    /**
    * �������� ������ ���������� ����������������� ����.
    * @access public
    * @var array
    * <code>
    *   [auth] => Array (
    *       [/root/auth/config/] => Array (
    *               [key] => 10
    *               [caption] => ������������
    *               [path] => /root/auth/config/
    *               [childs] => Array (
    *                        [/root/auth/defaults/] => Array (
    *                               [key] => 5
    *                               [caption] => �������� ������� �� ���������
    *                               [path] => /root/auth/defaults/
    *                           )
    *                   )
    *           )
    *   )
    *
    *   [...] => Array ( ... )
    * </code>
    */
    var $menu;

    /**
    * �������� ���������� ���������� � ���������������� ���� ������.
    * ���� ������ �� ������, �� �������� ������ ������.
    * @access public
    * @var string
    */
    var $selModule;

    /**
    * �������� ������ �� ��������� ������� ���� ������� ������.
    * ���� ������� �� ������, �� �������� NULL.
    * @access public
    * @var array
    */
    var $selLev1;

    /**
    * �������� ������ �� ��������� ������� ���� ������� ������.
    * ���� ������� �� ������, �� �������� NULL.
    * @access public
    * @var array
    */
    var $selLev2;

    /**
    * �������� �������������� �������� ������� �� ��������� {@link ADD_PATH} �� 2�� ������ �����������
    * @access public
    * @var array
    * <code>
    *   Array (
    *       [0] => admin
    *       [1] => errors
    *       [2] =>
    *   )
    * </code>
    */
    var $params;



    /**
    *  �����������.
    *  @access public
    *  @return void
    */
    function Admin_Menu() {
   		global $urls, $_VARS;

        $this->menu= array ();
        $this->cur_module= null;
        $this->CurLev1= null;
        $this->CurLev2= null;
        $this->params= $_VARS;

        /** ID ���� � ���������������� �������� */
        $this->main_path_ID= 2;

        /** ID ���� � ���������������� �������� */
        $this->selModule= '';

        /** ������� ���� ��� ��������� � �������� */
        $this->curPath= $urls->current;

        /** �������� ���� ���������� �������� ���� ������� ������. */
        $this->selLev1= null;

        /** �������� ���� ���������� �������� ���� ������� ������. */
        $this->selLev2= null;

        /** �������� ���� ���������� �������� ���� ������� ������. */
        $this->linkToLev1= null;


    }

    /**
    *  ���������� ����������������� ����.
    *  @access private
    *  @return void
    */
    function _makeMenu() 
		{
        global $_MODULES, $adminmenu,$auth_isROOT;
        static $called= false;
							 
        if ($called)
            return;

        $called= true;

        $mods= $_MODULES->info;		 

        foreach ($mods as $module) 
			{
            if (!$module['installed'])
                continue;

			$this->cur_module= $module;
			if ($auth_isROOT && !$_MODULES->isSystem($module['module_dir']))
				$this->Option('������������', 'config');
			UseModule($module['module_dir']);
			
			$filemenu= MODULES_DIR.EndSlash($module['module_dir']).'menu.php';	
			if (is_file($filemenu)) @include_once $filemenu;

			$this->cur_module= null;
			$this->CurLev1 === null;
			$this->CurLev2 === null;
	        }
		}

    /**
    *  ���������� ������ ���� ������� ������
    *  @access public
    *  @param string $caption �������� ������ ����
    *  @param string $path ������������� ����
    *  @param string $rights ����������� ����� ��� ������� � ������. ������������ ����� �������.
    *  @return void
    */
    function Option($caption, $path, $rights= '') {

        $caption= (string) $caption;
        $path= (string) $path;

        if (!$caption || !$path)
            return;

        $rights=true;
        $path= $this->_makePath($path);


        $selected= strpos($this->curPath, rtrim($path,'/')) === 0;

        if (!$rights)
            return;

        $this->CurLev1= & $this->menu[$this->cur_module['module_dir']][$path];

        if ($selected) {
            $this->selModule= $this->cur_module['module_dir'];
            $this->selLev1= & $this->CurLev1;
            $this->selLev2= null;
        }

        $this->CurLev1['key']= $this->_key();
        $this->CurLev1['caption']= $caption;
        $this->CurLev1['path']= $path;
        $this->CurLev1['childs']= array ();
    }






    /**
    *  ��������� ������ �� ������ �������� ����� ����.
    *  ���� ������, �� � ���������� {@link $this->selected_module} ���������� ���������� ���������� ������,
    *  @access private
    *  @param string $path ���� ����� ����
    *  @param integer $level ������� ����� ����
    *  @return boolean
    */
    function _checkForSelect($path, $level) {

        if (strpos($this->curPath, $path) === 0) {
            $this->selModule= $this->cur_module['module_dir'];
            unset ($this->selLev1);
            $this->selLev1= & $this->CurLev1;
            if ($level == 2) {
                unset ($this->selLev2);
                $this->selLev2= & $this->CurLev2;
                
            }

            return true;
        }

        return false;
    }

    /**
    *  ���������� ���������������� ����.
    *  @access private
    *  @return integer
    */
    function _key() {
        static $i= 0;
        return ++ $i;
    }

    /**
    *  ������� ���� � ������� ����.
    *  @access private
    *  @param string $path ���� ����� ����
    *  @return string HREF-����
    */
    function _makePath($path) {
		global $urls;
        $path= (string) $path;
        $path= str_replace('?', '&', str_replace('&amp;', '&', $path));
        $path= trim($path, '&');

        if (preg_match('#^([^&]*)&(.*)$#', $path, $matches= null)) {
            $path= $matches[1];
            $get= $matches[2];
        }
        else
            $get= '';

        return EndSlash(EndSlash($urls->path).$this->cur_module['module_dir']).EndSlash($path);

    }

}
?>