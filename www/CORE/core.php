<?php
//----------------------------------------------------------------------------------------------------------------------------------------
define('CORE_DIR', DOC_ROOT.'/CORE/');
require_once CORE_DIR.'config.core.php';


/** ������� ����������� */
require_once CORE_DIR.'includes/authorizate_functions.php';

/** ������ ������ */
require_once CORE_DIR.'classes/error_handling.php';

/** ��������� ������� � ���� */
require_once CORE_DIR.'configs/date.php';

/** ������ ��������� */
require_once CORE_DIR.'configs/other.php';

/** ��������� ���� */
require_once CORE_DIR.'configs/forms.php';


/** ���������� ������� */
require_once CORE_DIR.'includes/global_functions.php';
require_once CORE_DIR.'includes/images_functions.php';

//----------������� �����------------------------
require_once CORE_DIR.'includes/kses.php';

/** ������� ������� */
require_once CORE_DIR.'includes/mod_functions.php';


/** ���������������� ������� */
require_once CORE_DIR.'includes/user_functions.php';

/** ��������� output buffer */
ob_start('OB_handler');



/** XML2ARRAY */
require_once CORE_DIR.'classes/xmls.php';

/** ���������� ���������� */
require_once CORE_DIR.'configs/globals.php';

/** ���������� ����� ������ � ������ � �������� */
require_once CORE_DIR.'classes/urls.php';

/** ������������ � �� */
require_once CORE_DIR.'classes/db.php';

/** ���������� ����� ���������� ���� ������������ */
require_once CORE_DIR.'classes/configs.php';  

require_once CORE_DIR.'classes/sendmail.php';

/** ����������� ������ ��� ������ � ���������� */
require_once(CORE_DIR.'classes/Image_Toolbox.class.php');

/** ����������� ������ ������ � �������� � ������� */
require_once CORE_DIR.'classes/module_director.php';




/** ����� ������������� ���� */
$Forms = new form_configs();

/** ����� ���������� ������ � �������� */
$urls= new UrlsAndPaths();

/** ����� ��� ������ � ���������� */
$Images = new Image_Toolbox();

/** ����� ���� ������ */
$DB= new DB_Engine('mysql', $_CONFIG['DBHOST'], $_CONFIG['DBUSER'], $_CONFIG['DBPASS'], $_CONFIG['DBNAME']);

/** ����� ���������� �������� */
$_MODULES= new ModuleDirector();

/** ����� ���������� � ������� */
$_MODULES->getModulesInfo();



/** ���������� ���������� ������� */
unset ($_CONFIG, $DOC_ROOT);

/** ���������� ��������� ������ */
if (sizeof($_MODULES->sys_modules))
	foreach ($_MODULES->sys_modules as $m)
	    if ($m != 'admin')
			UseModule($m);
foreach($_MODULES->info as $module_name => $module_data){
	if ($module_data['installed']==1){
		UseModule($module_name);
	}
}

/** ���������� */
auth_Authorization();	 

$auth_isROOT=auth_isROOT();

require($_SERVER['DOCUMENT_ROOT'].'/CORE/includes/exec.funcs.php');

$current_query = CURRENT_QUERY;

if(isset($_REQUEST['print_version'])){
	$GLOBALS['print_version']=true;
	$current_query=str_Replace('?print_version=1','',$current_query);
}

//if(isset($_REQUEST['_openstat'])){
////	debug($current_query);
//}
//
$current_query = trim($current_query,'/');


/** ���� ���������� STOP_CMS �� ��������� - ���������� ������ (������ �� ������������� ����� �������) */
if (!defined('STOP_CMS')){
	/** ��������� �� ����� � ���������� ������� ���� */
	$page = $urls->Process_Current_Url(CURRENT_QUERY);


	/** ��������������� � ������� ���������� */
	if ( $page['root_only'] && (!auth_inGroups(array(1,2)) || !auth_getUserId() || auth_getUSerId()==1) )	
		goto('/'.ROOT_PLACE.'/auth/');

	if ( $page['rdr_path_id']>1)												
		goto("/".path($page['rdr_path_id']));

	if ( $page['rdr_url']!="")													
		header("Location: ".$page['rdr_url']);

	/** ����� ���������� � ����� ������� �� �� */
	$_ZONES= (array) $DB->getAll('SELECT * FROM `'.PRFX.'zones` ORDER BY id');

	/** �������� ����������, � ������� ����� ������ ���� html ����� �������� */
	$RESULT= '';

	
	/** ���������� ������ ��� TDK */
	
	
	$_TITLEPAGE = $page['title_page'];
	$_TITLEMENU = $page['title_menu'];
	
	//-------------------------------------������������-----------------
	
	if($page['path_id']>10){
	
//		debug(CURRENT_QUERY,0);
		
		$title_rules=$DB->GetAll($q='SELECT * FROM `'.PRFX.'titles` WHERE "/'.CURRENT_QUERY.'/" LIKE  `url` ORDER by `sort` ASC');
		if(count($title_rules)>0){
			
			$r_title=$_TITLEPAGE;
			foreach ($title_rules as $tr){
				$r_title=makeChangeTitle($_TITLEPAGE,$tr['text'],$r_title,$tr['type_insert']);
			}
		$_TITLEPAGE=$r_title;
		}
	}	
	//-------------------------------------������������-----------------
	
	if (empty($_TITLEPAGE)) $_TITLEPAGE = $_TITLEMENU;
	
	$_META_DESCRIPTION = $page['meta_description'];
	$_META_KEYWORDS = $page['meta_keywords'];

	/*$atdk=array();	   
	if ($_META_DESCRIPTION!=""){
		$atdk[]='<meta name="description" content="'.$_META_DESCRIPTION.'">';
	}

	if ($_META_KEYWORDS!=""){
		$atdk[]='<meta name="keywords" content="'.$_META_KEYWORDS.'">';
	}*/
	    

	/** �������� �� ���� ������������ ����� */
	foreach ($_ZONES as $values) {
		$zone = $tmp_zone = $values['value'];

		$search=false;
		if (isset($values['for_search']) && $values['for_search'] == 1) $search=true;
		
		
		/** ��������� ����������� ���������� ���� */
		global $$zone;

		if (!isset ($$zone)) $$zone = '';

		$cur_zone_content = $DB->getOne('SELECT content FROM `'.PRFX.'zones_content` WHERE zone_id="'.$values['value'].'" AND path_id="'.$page['path_id'].'" LIMIT 1');
		$cur_zone_content = html_entity_decode($cur_zone_content, ENT_QUOTES);

		/** ��� ������� � ���� */
		if (!isset($page['config'][$zone])) {$page['config'][$zone]=array();}
		
		//if ($zone == '_CONTENT_') 
		//	$$zone = isset($page['content']) && $page['content']!="" ? $cur_zone_content.$page['content'] : $cur_zone_content;
		//else 
			$$zone = $cur_zone_content;
		
		$parts = $page['config'][$zone];

		/** ���� ���� �� �������� ������, �� ���������� */
		if (sizeof($parts) == 0){
			$$zone = '<!--[INDEX'.(isset($values['search_links']) && $values['search_links']==1 ? '*' : '').']-->'.($$zone!="" ? $$zone : '').'<!--[/INDEX]-->';
			continue;
		}

		/** ����������� �� ���� ������ � ��������� �� ��� � ������������ � �������� ���������� �������. */
		foreach ($parts as $part_template) {
			$result_text= '';
			
			/**
			*  ���� ��� ������� ����� ����� ���� �������� �����-�� ����������,
			*  �� ��� � �������
			*  @see UrlsAndPaths::set_vars()
			*/
			if ($part_template['vars'] != ''){
				$urls->set_vars($part_template['vars']);
			}

			/** ���� ������ ������, �� ��������� ��� */
			if ($part_template['script'] != '') {
				if (is_file(DES_DIR.$part_template['script'])) include (DES_DIR.$part_template['script']); 
				$result_text= ob_get_contents();
				@ob_clean();
			}
			/**
			*  ���������� ������� ����������� ����������
			*  @see UrlsAndPaths::unset_vars()
			*/
			if ($part_template['vars'] != '')
				$urls->unset_vars($part_template['vars']);

			/** ��������� � ������ ��������� */
			$$zone .= $result_text;

			unset ($result_text);
			}

		if ($$zone=="") 
			$$zone = isset($page['content']) && $page['content']!="" ? $page['content'] : '�������� ��������� � ������ ����������';
		
		if ($search) 
			{
			$$zone = '<!--[INDEX'.(isset($values['search_links']) && $values['search_links']==1 ? '*' : '').']-->'.$$zone.'<!--[/INDEX]-->';
			}
		}


	$print_version=false;
	if (isset($_REQUEST['print'])){
		ob_clean();
		 $vars['_CONTENT_'] = $_CONTENT_;
		 $vars['_CONTENT_ADD_'] = $_CONTENT_ADD_;
		 $vars['_NEWS_'] = $_NEWS_;
		 $vars['_TITLEPAGE'] = $_TITLEPAGE;
		 $vars['_META_KEYWORDS'] = $_META_KEYWORDS;
		 $vars['_META_DESCRIPTION'] = $_META_DESCRIPTION;
		 $vars['_TITLEMENU'] = $_TITLEMENU;
		 die (template('print',$vars));

		$print_version=true;
	}

	/** ���������� �������� ������ */
	if (is_file(DES_DIR.$page['main_template'])) {
		require_once (DES_DIR.$page['main_template']);
		$RESULT= ob_get_contents();
		
		ob_clean();
		
	} else  {
		die('�� ������ �������� ������ ������� ��������');
	}

	/** �������� ������������ ������ */
	$ct_header = 0; // ���� - ��� �� ������ ��������� Content-Type
	$header_list = headers_list();
	foreach($header_list as $head){
	 if(substr($head,0,13) == 'Content-Type:') $ct_header = 1;
	}
	if($ct_header == 0) header('Content-Type: text/html; charset=windows-1251');
	
	/** ������� ���� ����� TDK */
	AddBeforeTag((sizeof($atdk) ? implode("\n",$atdk)."\n" : ''),'<title>');	
	unset($atdk);
	
	/** ��������� �������� */
	echo Append2HTML($RESULT);
}

auth_StoreSession();
?>