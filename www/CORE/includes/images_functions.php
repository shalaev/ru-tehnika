<?
function makeImageThumbs($data, $upload_files, $config_name, $create_sys_thumbs=false){
	global $Images;

	if (isset($upload_files[$config_name]['error']) && $upload_files[$config_name]['error']== 4){
		$data['value'] = isset($result['path']) ? serialize($result['path']) : '';
		return false;
	}

	if (isset($upload_files[$config_name]['error']) || (isset($data['allowed']) && !isset($data['allowed'][ExtractExt($upload_files[$config_name]['path'])]))){
		if (isset($upload_files[$config_name]['error']) && $upload_files[$config_name]['error']!="")
			debug($upload_files[$config_name]['error'],1,0,'',false);
		else
			debug('� ���������������� ���� "<b>'.$config_name.'</b>", ����������� ���� ����� �� ���������� ����������. �������� ������: <b>'.implode(',',$data['allowed']).'</b>',1,0,'',false);

		$data['value']='';

		return false;
	}
	
	$temp=array();
	
	$upload_file_name=$upload_files[$config_name]['path'];
	
	$upload_file_name_fs="/".trim(DOC_ROOT,"/").$upload_files[$config_name]['path'];

	$folder = explode(':',date('Y:m:d',NOWTIME));
	$folder = FILES_DIR.EndSlash(implode('/',$folder));
	
	//-------�������� �������---------
	list($w0,$h0)=getimagesize($upload_file_name_fs);
					
	foreach($data['thumbs'] as $th_name => $th){
		//----������� ��������� ���������� �����
		$w1=$th[0];
		$h1=$th[1];
		
		$crop=isset($th[2])?$th[2]:false;
		
		$new_name = explode('.',basename($upload_files[$config_name]['path']));
		$new_name[0] = $new_name[0].'_'.$w1.'_'.$h1;
		$new_name = uniqFile($folder.implode('.',$new_name));
		
		//--------�������, ��� ������� �������� ������ ����������� �� ����
		if(
			($w1==0 && $h1==0) || 							//----������� ���� - ���������� ���������
			($w1!=0 &&  $h1!=0 && $h0<=$h1 && $w0<=$w1) || 	//----������ ������������� ��� ���������������, �� �������� ����������� ������ ��������������
			($h1==0 && $w0<=$w1) || 						//----������� ��������������� �� ������, �� ��� ������ ���������
			($w1==0 && $h0<=$h1) 							//----�� ��, ������ �� ������
		){
			
			//-----��������� ��������----------
			$z=copy($upload_file_name_fs,$new_name);
			
		} else {
			//------������� ������---------------------------
			
			if($w1!=0 && $h1!=0 && !$crop){
				//-----���� ��������� � �������������-------
				
				
	
				//-----��������� �����������-----------------
				$p0=$w0/$h0;
				$p1=$w1/$h1;
				
				if($p0>$p1)$h1=0;
				else $w1=0;
				
			}
			
			$Images->newImage($upload_file_name_fs);
			$Images->newOutputSize($w1,$h1,$crop, false);
			
			$r=$Images->save($new_name,'jpg');
		}
		
		chmod($new_name,0777);
		$temp[$th_name] = str_replace(DOC_ROOT,'/',$new_name);

	}
	
	//----������� ��������� ����������� ���� ����
	if($create_sys_thumbs){
		$temp['sys_thumb'] = str_replace(DOC_ROOT,'/',makeSysImageThumb($upload_file_name));
	}
	
	//----������� ���������
	unlink($upload_file_name_fs);
	
	$data['value'] = serialize($temp);		
	
	if (isset($temp) && is_array($temp))
		$data['value'] = serialize($temp);

	return $data['value'];
}

function deleteAllimagesBySerialize($str){
	$images=unserialize($str);
	foreach ($images as $img){
		if($img!='' && file_exists($_SERVER['DOCUMENT_ROOT'].$img))unlink($_SERVER['DOCUMENT_ROOT'].$img);
	}
}


function getImageURL($images, $num){
	if ($images!=""){
		$s = html_entity_Decode($images,ENT_QUOTES);
		$s = unserialize($s);

		return isset($s[$num]) ? $s[$num] : '';
	} else return '';
}

function getImageHTML($images, $num=1, $title='', $params=''){
	$s = getImageURL($images, $num);
	
	if($s=='' || !file_exists($_SERVER['DOCUMENT_ROOT'].$s))return '';

	$filename = $_SERVER['DOCUMENT_ROOT'].'/'.$s;
	$filename = str_Replace('//','/',$filename);

	list($w,$h) = getimagesize($filename);

	return '<img src="'.$s.'" width="'.$w.'" height="'.$h.'" alt="'.$title.'" title="'.$title.'" border="0" '.$params.'>';
}

function getImageURLFromCollection($images,$item_id,$num){
	if ($images!=""){
		$s = html_entity_Decode($images,ENT_QUOTES);
		$s = unserialize($s);  
		return isset($s[$item_id]['path'][$num]) ? $s[$item_id]['path'][$num] : '';
	} else return '';
}

function getImageHTMLFromCollection($images, $item_id, $num=1, $title='', $params=''){
	$s = getImageURLFromCollection($images, $item_id, $num);
	
	if($s=='' || !file_exists($_SERVER['DOCUMENT_ROOT'].$s))return '';

	$filename = $_SERVER['DOCUMENT_ROOT'].'/'.$s;
	$filename = str_Replace('//','/',$filename);

	list($w,$h) = getimagesize($filename);

	return '<img src="'.$s.'" width="'.$w.'" height="'.$h.'" alt="'.$title.'" title="'.$title.'" border="0" '.$params.'>';
}



function makeSysImageThumb($orig_filename){
	global $Images;

	$folder = explode(':',date('Y:m:d',NOWTIME));
	$folder = FILES_DIR.EndSlash(implode('/',$folder));
	
	/** ������ ��������� ��� ����� � ������������� � ������� */
	$new_name = explode('.',basename($orig_filename));
	$new_name[0] = $new_name[0].'_75_0';
	$new_name = $folder.implode('.',$new_name);
	   
	$Images->newImage(DOC_ROOT.$orig_filename);
	$Images->newOutputSize(75,0,false,false);
	$Images->save($new_name,'jpg');   
	 
	chmod($new_name,0777);
	
	return $new_name;
}

function getSysImage($value){
	$sys_img='';
	$imgparts=array();

	if (isset($value)) 
		$imgparts=explode('.',$value); 

	if (sizeof($imgparts)==2)
		$sys_img=$imgparts[0].'_75_0.'.$imgparts[1];

	if (isset($value) && $sys_img == $value)
		{
		$sys_img = @explode('.',$value);

		if (is_Array($sys_img) && sizeof($sys_img)>0 && isset($sys_img[1]) && isset($sys_img[0]))
		   $sys_img = $sys_img[0].'_75_0.'.$sys_img[1];
		}

	if (!is_file(DOC_ROOT.$sys_img)) 
		$sys_img = '';			  

	return ($sys_img) ? $sys_img : '';
}
?>