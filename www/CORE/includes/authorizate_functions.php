<?php

/**
*  ���������� ���������� �� �������� ������������.
*  ���� ������������ ($user_id) �� ������, �� ������������ ������� ������������.
*  @access public
*  @param integer $user_id ���������� ����� ������������
*  @param boolean $recache �������� ���
*  @return false|array ���������� � ������������
*/
function auth_UserInfoById($user_id= null, $recache= false) {
    global $_USER;

    $recache= (boolean) $recache;

    /** ���� ������ id �������� ������������ */
    if ($user_id === null || $user_id == $_USER['user_id'])
        return $_USER;

    $users= auth_GetUsers($recache);
	
    if (!isset ($users[$user_id]))
        return false;

    $user= $users[$user_id];

    $g= $user['groups'];
    $u= $user;
    $user['groups']= ($user['groups'] != '') ? $user['groups'] : array ();
	$user['profile'] = ($user['profile'] != '') ? $user['profile'] : array();

    unset ($users);

    return $user;

}

/**
*  ���������� ���������� �� �������� ������������.
*  ���� ������������ ($username) �� ������, �� ������������ ������� ������������.
*  @access public
*  @param string $username ���������� ��� ������������
*  @param boolean $unsensetive Case-����������. ������������ �������������� ������ � ��.
*  @return false|array ���������� � ������������
*/
function auth_UserInfoByName($username= null, $unsensetive= false) {
    global $DB, $_USER;
    static $users= null;

    /** ���� ������ id �������� ������������ */
    if ($username === null)
        return $_USER;

    if ($users === null)
        $users= assoc('username', auth_GetUsers());

    if ($unsensetive) {
        $result= $DB->getOne("SELECT `username` FROM `".PRFX."users` WHERE `username` LIKE '".$username."' ");
        if ($result)
            $username= $result;

        unset ($result);
    }

    if (!isset ($users[$username]))
        return false;

    $user= $users[$username];

    $user['groups']= ($user['groups'] != '') ? unserialize($user['groups']) : array ();

    return $user;

}

/**
*  ������� ���������� ��������������� ������ � ����������� � ���� �������������.
*  ������������ ������ � ������ ������������ �� �������� ��������������.
*  ������������ ������ �������� � �������� KEY - ID �������������, 
*  � � �������� VALUE ������ ������������
*  @access public
*  @param boolean $recache ����� �� �� �����
*  @return array
*/
function auth_GetUsers($recache= false) {
    global $DB;
    static $users= null;

    if ($users === null || $recache)
        $users= assoc('user_id', $DB->getAll("SELECT * FROM `".PRFX."users` ORDER BY `user_id`"));

    return $users;

}

/**
*  �������������� �������� ������������
*  @access public
*  @exception �� �����������, ���� ������������ �������� �������.
*  @return boolean ���������� false, ���� ������������ ��� �����
*/
function auth_LogOut() {
    global $DB, $_USER;

    if (!GUEST && $_USER['robot'] == 0) {

		$sql="DELETE FROM `".PRFX."users_sessions` WHERE `user_id`='".(int)$_USER['user_id']."'";
		$DB->execute($sql,false,false);

		//$_USER['user_id']= 1;
		
		setcookie(SESS_NAME."_auth",'',time()+1,'/');

        return true;
    }

    return false;
}


	

/**
*  ����������� ������������� ������������.
*  @access public
*  @param string $login_email ����� ������������ ��� email ������������. 
*                             � ����������� �� �������� ������������� TYPE_AUTH �����
*                             ������.
*  @param string $pass ������ ������������
*  @param integer $remember_me (0|1) ���� 0 - ���������� ������������ ��������� �� �������� ���� ��������
*  @exception ���������� Err1, ���� ������������ � ����� ������� � ������� �� ����������  
*  @exception ���������� Err2, ���� ������������ �� ����� ����� "CAN_AUTH"  
*  @return error_code|true ������ ���������� �������
*/
function auth_LoginUser($login, $pass, $remember_me) {
    global $DB, $_USER;

    $login= (string) $login;
    $pass = (string) $pass;
    $remember_me= $remember_me ? 1 : 0;

    $_USER['remember_me']= $remember_me;
	
    
    $sql= "`username` LIKE '".$DB->prepare($login)."'";

    $sql= "SELECT `user_id` FROM `".PRFX."users` WHERE ".$sql." AND `pass` = '".md5($pass)."' ";
    $user_id= $DB->getOne($sql);

 
    
    if (!intval($user_id))
        return 'Err1';
        
    $user= auth_UserInfoById($user_id);


    /** last_active �� �� ������, ���� last_active � ���������� ������������ �� ����� ���� */
    if ($user['last_active'])
        unset ($_USER['last_active']);

    /** last_visit ����� �� ������ ���� � ���������� ������������ ��� ������� */
    if ($user['last_visit'])
        unset ($_USER['last_visit']);

    /** ��� ������������ ������ �� �������� ������������ */
    unset ($_USER['username']);

    /** �� � ID ������������ �������, ������� */
    unset ($_USER['user_id']);
    unset ($_USER['pass']);

    $_USER= array_merge($user, $_USER);

	$_USER['groups'] = $user['groups'];
	$_USER['profile'] = $user['profile'];
    $_USER['user_ip'] = REMOTE_ADDR;

    
    if ($remember_me) 
    	setcookie(SESS_NAME."_auth", $_USER['sess_id'], time()+365*60*60*24,'/');

    unset ($user);
    
    return true;
}

function auth_validUser($username)
	{
	global $DB;
	
	return $DB->getOne('SELECT 1 FROM mp_users WHERE username="'.$DB->pre($username).'" AND user_id>5');
	}

/**
*  ������� ������ ������������. �����������.
*  @param string $login ����� ������������
*  @param string $pass ������ ������������
*  @param integer $remember_me (0|1) ���� 0 - ���������� ����������� ��������� �� �������� ���� ��������
*  @access public
*  @return true|string ������ ���������� ������� ��� ����� ������
*/
function auth_CreateUser($login, $pass, $email, $remember_me) {
    global $DB, $_USER;

    $login= (string) $login;
    $pass= (string) trim($pass);
    $email= (string) $email;
    $remember_me= $remember_me ? 1 : 0;

    /** �������� �� ���������� email ��� ����� ������������ */
    $result= $DB->getRow("SELECT username, useremail FROM ".PRFX."users WHERE username = '".$DB->prepare($login)."'". ($email !== null ? " OR useremail = '".$DB->prepare($email)."' " : ""));

    /** ������� ���������� */
    if (sizeof($result)) {
        if ($result['username'] == $login)
            return '� ���������, ��� &laquo;%s&raquo; ��� ������������. ����������� ������ �����.|'.$login.'';
        if ($result['useremail'] == $email)
            return '� ���������, email &laquo;%s&raquo; ��� ������������ ����� �� �������������. ����������� ������ E-mail.|'.$email.'';
    }
    unset ($result);

    /** ������� ������ */
    $sql= array ();

    $sql[]= "username = '".$DB->prepare($login)."'";

    if ($email !== null)
        $sql[]= "useremail = '".$DB->prepare($email)."'";

    $sql[]= "pass = '".md5($pass)."'";
    $sql[]= "regdate = '".NOWTIME."'";

    $DB->execute("INSERT INTO ".PRFX."users SET ".implode(", \n", $sql)." ",true,false);
    unset ($sql);

    return true;
}

/**
*  �������� ������������ �� �������
*  @access public
*  @param integer $user_id ID ������������
*  @return boolean
*/
function auth_DeleteUser($user_id) {
    global $DB;

    $user_id= (integer) $user_id;
    if ($user_id === 1 || $user_id === 0)
        return false;

    $DB->execute("DELETE FROM `".PRFX."users` WHERE `user_id` = '".$user_id."' ");
    $DB->execute("DELETE FROM `".PRFX."users_sessions` WHERE `user_id` = '".$user_id."' ");

    return true;
}

/**
*  ��������� ������ ����������� ID ������
*  @access private
*  @return string sess_id
*/
function auth_GenerateSID() {
    global $DB;

    $md5= md5(microtime().getmypid().mt_rand(1, 100000));
    $sql= "SELECT `user_id` FROM ".PRFX."users_sessions WHERE sess_id = '".$md5."' ";
    $result= $DB->getOne($sql);
    return (!empty ($result)) ? auth_GenerateSID() : $md5;
}

/**
*  ����� ���������� �� �����, ���� ������ �����, ��
*  ���������� ������ ����������
*  @access private
*  @return array SessionInfo
*  @param string $sess_id ID ������
*
*/



function auth_GetInfoSession($sess_id) {
    global $DB, $urls;
    static $test= null;

    $sql= "SELECT * FROM `".PRFX."users_sessions` WHERE `sess_id` = '".$DB->prepare($sess_id)."' ";

    $session= $DB->getRow($sql);

    if (empty ($session)) {

        /** ��� ������������ �������� ����� ������ */
        if (!defined('NEW_USER')) {
            define('NEW_USER', true);
            
        }
        $session['user_id']= 1;
        $session['sess_id']= $sess_id;
        $session['user_ip']= REMOTE_ADDR;
        $session['last_active']= NOWTIME;
        $session['last_visit']= 0;
        $session['remember_me']= 1;
        $session['enter_to_site']= NOWTIME;

    }
    elseif (!defined('NEW_USER')) define('NEW_USER', false);

    $session['cur_path']= $urls->current;

    return $session;
}


/**
*  ���������� � ������������ ��� ������ ����������, ������������ ��-���������, 
*  ���� ��������� ������������ �� ����������.
*  @access public
*  @param integer $user_id ID ������������
*  @param boolean $recache �������� ���
*  @return array UserInfo
*/

function auth_GetUserProfile($user_id)
	{
	global $DB;
	
	return $DB->getRow('SELECT * FROM mp_users_profiles WHERE user_id="'.$DB->pre((int)$user_id).'"');
	}

function auth_GetInfoUser($user_id, $recache= false) {
    global $DB;

    $recache= (boolean) $recache;

    $user= auth_UserInfoById($user_id, $recache);
    
    if ($user === false && $user_id === 1)
		{
		echo '������ ������� CMS: �� ������� �������� ������������';
		echo '<br>';
		echo '<br>';
		echo '��������� �������� ������������ �� ���������';
		
		$sql='';
		$sql.="
			INSERT INTO `mp_users` 
				(`user_id`, `robot`, `username`, `useremail`, `user_ip`, `pass`, `groups`, `profile`, `temp_info`, `regdate`, `last_active`, `last_visit`, `enter_to_site`, `ban_expired`, `ban_path`, `sex`, `_update`) 
			VALUES 
				(1, '0', 'guest', NULL, '',          '',				 NULL, '',       '',	   0,0,0,0,0,'','male',0), 
				(2, '0', 'root',  NULL, '127.0.0.1', '".md5('12345')."', NULL, 'a:0:{}', 'a:0:{}', 0,1202193857,1202193856,1202193857,0,'','male',0),
				(5, '0', 'admin', NULL, '127.0.0.1', '".md5('12345')."', NULL, 'a:0:{}', 'a:0:{}', 0,1202190521,1202190521,1202190521,0,'','male',0);
			";

		$DB->execute($sql);
		goto("/");
		exit;
		}

    elseif ($user === false) $user= auth_UserInfoById(1, $recache);

    return $user;
}

/**
 *  �������� � ���� ��������� � ������ ������������
 *  @access private
 */
function auth_StoreSession() {
    global $DB, $urls, $_USER;
    static $called= false;

    if ($called)
        return;
    else
        $called= true;

    $user_id= intval($_USER['user_id']);
    $sess_id= $DB->pre(session_id());
    
    $sql= array ();
    $sql[1]= "`username` = '".$DB->prepare($_USER['username'])."'";
    $sql[2]= "`user_ip` = '".$DB->prepare(REMOTE_ADDR)."'";
    $sql[3]= "`pass` = '".$DB->prepare($_USER['pass'])."'";
  //  $sql[5]= "`profile` = '".$DB->prepare(serialize($_USER['profile']))."'";
    $sql[5]= "`profile` = ''";
    $sql[6]= "`temp_info` = '".$DB->prepare(serialize($_SESSION))."'";
    $sql[7]= "`last_active` = '".NOWTIME."'";
    $sql[8]= "`last_visit` = '".intval($_USER['last_visit'])."'";
    $sql[9]= "`enter_to_site` = '".intval($_USER['enter_to_site'])."'";
    $sql[12]= "`user_id` = '".$user_id."'";
    $sql[13]= "`remember_me` = '".intval($_USER['remember_me'])."'";
    $sql[14]= "`cur_path` = '".$DB->prepare($urls->current)."'";
    $sql[16]= "`sess_id` = '".$DB->pre($sess_id)."'";

    /** ���� ���������������� ������������, �� ��������� ������ ���������� ������������ */
    if ($_USER['user_id'] != 1) {
        $_sql= $sql;
        unset ($_sql[12], $_sql[13], $_sql[14], $_sql[15], $_sql[16]);
        $_sql= "UPDATE `".PRFX."users` SET ".implode(",\r\n", $_sql)." WHERE `user_id` = '".$user_id."' ";
        $DB->execute($_sql,true,false);
    }

    /**
    *  ���� ����� ������������, �� ��������� ����� ������ � ����.
    *  ���� ��� ������������ ������, �� ��������� ��.
    */
    $_sql= $sql;
    unset ($_sql[3], $_sql[10], $_sql[11], $_sql[5]);

	if ($_USER['user_id'] != 1)
	{
	    if (NEW_USER)
	        $_sql= "INSERT INTO `".PRFX."users_sessions` SET  ".implode(",\r\n", $_sql);
	    else {
	        $_sql= "UPDATE `".PRFX."users_sessions` SET ".implode(",\r\n", $_sql)." WHERE `sess_id` = '".$sess_id."' ";
    	}
	    $DB->execute($_sql,true,false);
	}
    /** ���������� ������ ���������� */
    unset ($_sql, $sql, $sess_id, $user_id);
}


/**
*  ���������� �� ������������ ������.
*  ���������� �������� ROOT, GUEST, REGISTERED, BANNED, NEW_USER, ROBOT.
*  ������� ����������� ���� ��������, � ����.
*  @access public
*  @return void
*/
function auth_Authorization() {
    global $_USER, $DB;

    /** ������� ������ � ���������� ����������� */
    $_SESSION = array ();

    /**
    *  �������� id ������, ����� ��� ���������� ���
    *  @see auth_GenerateSID()
    */
    
    $cookie_sess_id = isset($_COOKIE[SESS_NAME."_auth"]) ? $_COOKIE[SESS_NAME."_auth"] : auth_GenerateSID();
    $sess_id= isset ($_COOKIE[SESS_NAME]) && $_COOKIE[SESS_NAME]!="" ? $_COOKIE[SESS_NAME] : $cookie_sess_id;

    /** ���� ������������ ID ������, �� ��� � �������� */
    if (!preg_match('#\A[A-Z0-9]{32}\z#i', $sess_id))
        die('������� ������ ����� ������� c������������� ��� ������: \'%s\' |'.$sess_id.'');

    /**
    *  ����� ���������� � ������ � ��������� ��������� NEW_USER = true,
    *  ���� ������������ ���� ������ ��� ������� ����� ������. � ��������� ������
    *  NEW_USER = false
    *  @see auth_GetInfoSession();
    */

    $session= auth_GetInfoSession($sess_id);
    
    /**
    *  ���� �� ��������� IP, �� ������� ����� �������� ������ ��� ��������
    *  @see auth_GetInfoSession()
    *  @todo ���������� ��������.. ���� ��� �� ������� �������. ������������ � ������������ ��������� IP 0.0.0.0 
    */
    if (CHECK_UNIQ_IP==1)
		if ($session['user_ip'] && $session['user_ip'] != REMOTE_ADDR)
	    {
	    	$DB->execute("DELETE FROM `".PRFX."users_sessions` WHERE `sess_id` = '".$DB->pre($session['sess_id'])."'",true,false);
	        $session= auth_GetInfoSession(auth_GenerateSID());
	    }
    /**
    *  ����� ���������� � ������������ � ���������� � ����������� �� ������.
    *  ����������� ����� ��������� ������, ��������� ��� ���������.
    *  ������ ��������� ������������ ��������� �� ����������.
    * 
    *  ���� ����� ��������� ������� �� ������ �����, ���� ������������������,
    *  �� �� ��� ����������� ��������
    */
    
    if ($session['user_id'] != 1) {
        $_USER= auth_GetInfoUser($session['user_id']);
        $_USER['remember_me']= $session['remember_me'];
        $_USER['sess_id']= $session['sess_id'];
        $_USER['cur_path']= $session['cur_path'];
    }
    else {
        $_USER= array_merge(auth_GetInfoUser(1), $session);
     
    }

    /** ��������� ������ ���������, ��� ����������� ������������� */

    /** ������������ ���������������. */
    define('REGISTERED', $_USER['user_id'] != 1);

    /** 
    *  ������������ �� ��������������.
    */
    define('GUEST', $_USER['user_id'] == 1);

    /** 
    *  ������������ �������� � ����� ���������� ����� � ��� �� ���� �����.
    */
//    define('ROOT', in_array($_USER['user_id'], (array) @ explode(',', ROOT_USER_IDS)));
//    define('ADMIN', in_array($_USER['user_id'], (array) @ explode(',', ADMIN_USER_IDS)));

    
    
    /**
    *  ������������ ���� ����������� ������ (last visit) �
    *  ��������� ���������� ������������ � ������� �����
    */
    /** ������� ����� ��������� ����� ����� ������� ������� ���������� */
    $_USER['last_visit']= $_USER['last_active'];

    /** ����� ����� �� ���� - ������� */
    $_USER['enter_to_site']= NOWTIME;

    $_USER['last_active']= NOWTIME;

    /**
    *  ��������� ������ � ������ id-������, ������ �����.
    *  ����������� ��������� ������ � �������� ������� � ���������
    *  ���������� �� proxy (� ����� ������������ �����������)
    */
    session_name(SESS_NAME);
    session_id($_USER['sess_id']);
    header('Cache-Control: private, proxy-revalidate, s-maxage=0');
    header('Pragma: no-cache');

    /**
    *  ��������� cookies. ���� ������������ ������� �� ��������� ����
    *  ($_USER['remember_me'] = 0), �� ����� ����� ����� �� �������� ���� ��������,
    *  � ��������� ������ ��������������� ����������� ������
    */

    $expire_time= ($_USER['user_id'] == 1) ? 900 : 3000000;

    if ($_USER['remember_me'] == 0)
        $expire_time= 0;

    /**
    *  ��������� ��������� cookies
    *  @see auth_SetCookie()
    */
    auth_SetCookie(SESS_NAME, session_id(), $expire_time);

    /** 
    *  ������������ �������� �������, �.�. �� �� ������ ����� ���� �� 
    *  �����-���� ���������������� �������� � ������� ��� �����, ��� ����������� ��
    *  ���� ����� �� ��� ����� �����.
    */
    define('ROBOT', (boolean) $_USER['robot']);

    /** ���� ������������. 2�-��������� ��������� �����. */
	//define('LANG', lang_UserLang());

    
    
	session_start();

	if (isset($session['temp_info']))
		{
		$mass = unserialize($session['temp_info']);	
		if (is_array($mass))
			foreach ($mass as $key => $value)
				$_SESSION[$key] = $value;
		}

	/** ����������� ������ �������������� ���������� */
	unset ($expire_time, $session, $sess_id);
	}

/**
* ������������� cookies ��� ���� ��������� � ���������������� ����� ����� �������� ����.
* @param  string $_var ��� ���������� cookie
* @param  string $_value �������� ���������� cookie ��� ������ ������
* @param  integer $_expire ���� ��������� ����� ����� ����������
* @return void
*/
function auth_SetCookie($_var, $_value= '', $_expire= 0) {
    

    $domains[]= DOMAIN;
    $domains[]= '';

    if ($_expire != 0)
        $_expire += NOWTIME;

//    foreach ($domains as $domain)
    setcookie($_var, $_value, $_expire, '/');

    unset ($domains, $domain);
}

/**
*  �������� ������ ��������� ������������.
*  @access public
*  @param integer $user_id ID ������������
*  @param array $userdata ������ ������������. ��������������� ������.
*  @return void
*/
function auth_SetUserInfo($user_id, $userdata) {
    global $DB;

    $sql= array ();
    $userdata= (array) $userdata;
    $user_id= (integer) $user_id;

    if (!sizeof($userdata))
        return;

    foreach ($userdata as $key => $value) {

        if (is_array($value))
            $value= _serialize($value);
        else
            $value= $DB->pre($value);

        $sql[]= "`".$DB->pre($key)."` = '".$value."' ";

    }
    $DB->execute("UPDATE `".PRFX."users` SET ".implode(",\r\n", $sql)." WHERE `user_id` = '".$user_id."' ",true,false);
}

/**
*  ���������� ������ ����������� ������.
*  ���� ����� ������ �� �������, �� ������������ ����������� ����� ������. 
*  @access public
*  @param integer $len ������ ������������� ������
*  @return false|string ������ ��� false ���� �������� ������, ������ ������������.
*/
function auth_GenPass($len= null) {

	global $auth_isROOT;
    if ($len === null)
        $len= getConfig('MIN_PASS_SIZE');
    elseif ((!$auth_isROOT && $len < 5)) return false;

    /** ������� */
    $vv= 'aeiouy';
    $vv .= '08';

    /** C�������� */
    $cs= 'bcdfghjkmnpqrstvwxz';
    $cs .= '12345679';

    $res= '';
    for ($i= 0, $l= floor($len / 2); $i < $l; $i ++)
        $res .= $cs[mt_rand(0, strlen($cs) - 1)].$vv[mt_rand(0, strlen($vv) - 1)];

    if ($len % 2)
        $res .= $cs[mt_rand(0, strlen($cs) - 1)];

    unset ($vv, $cs, $i, $l, $len);
    return $res;
}

/**
*  ���������� ���������� �� ���� �������������. ����� ������� ������������� ������� �����.
*  ��� ����������� �������� � ������ ������������ ������ "*".
*  ������������ ������ ������������ �� ������ ������������� � ������������ �������.
*  @access public
*  @param string $mask �����
*  @return array
*/
function auth_UsersByMask($mask) {
    global $DB;

    $mask= (string) $mask;
    return (array) $DB->getAll("SELECT * FROM `".PRFX."users` WHERE `username` LIKE '".$DB->prepare(str_replace('*', '%', $mask))."' AND `user_id` != '1' ORDER BY `username`");

}

function auth_Access($module_name, $action, $user_id=0){
	global $DB;

	if ($user_id==0) $user_id = auth_getUserId();

	switch ($action){
		default:
			$access=false;
		break;

		case 'add':
		case 'edit':
		case 'del':
		case 'move':
		case 'link':
		case 'vis':
			$module_id=$DB->GetOne("SELECT `module_id` FROM `".PRFX."modules` WHERE `module_name`='".$DB->pre($module_name)."'");
			$sql='SELECT `acl_'.$action.'` FROM `'.PRFX.'users_access` WHERE `user_id`="'.(int)$user_id.'" AND CONCAT(",",access_module,",") LIKE "%,'.(int)$module_id.',%"';
//			$sql='SELECT `acl_'.$action.'` FROM `'.PRFX.'users_access` WHERE `user_id`="'.(int)$user_id.'" AND module="'.$module_name.'"';
			$data = $DB->getRow($sql);

			if (sizeof($data)==1) 
				$access = (boolean)$data['acl_'.$action]; 
			else 
				$access = (boolean)DEFAULT_ACCESS;
		break;
	}
	
	return $access;
}

function auth_access2Module($module_id, $user_id=0){
	global $DB;

	if ($user_id==0) $user_id = auth_getUserId();
	
	return (int)$DB->getOne('SELECT 1 FROM mp_users_access WHERE CONCAT(",",access_module,",") LIKE "%,'.(int)$module_id.',%" AND user_id="'.(int)$user_id.'"');
}

function auth_isROOT($user_id=0){
	global $DB;

	if ($user_id==0) $user_id = (int)auth_getUserId();

	$sql = 'SELECT 1 FROM mp_users WHERE CONCAT(",",groups,",") LIKE "%,1,%" AND user_id="'.(int)$user_id.'"';
	
	$acc = (int)$DB->getOne($sql);

	return $acc==1 ? 1 : 0;
}

function auth_UserSessId(){
	if (isset($_COOKIE[SESS_NAME]) && $_COOKIE[SESS_NAME]!="")
		{
		return $_COOKIE[SESS_NAME];
		}
	elseif (isset($_COOKIE[SESS_NAME."_auth"]) && $_COOKIE[SESS_NAME."_auth"]!="")
		{
		return $_COOKIE[SESS_NAME."_auth"];
		}
	else
		{
		return '';
		}
}
	
function auth_UserIsLogin()
	{
	$sess_id = auth_UserSessId();
	$info = auth_GetInfoSession($sess_id);
		
	return (boolean)($info['user_id']>5);
	}

function auth_getSiteUserId()	
	{
	return auth_getUserId(false);
	}
	
function auth_getUserId($is_adminka=true)
	{
	$array = auth_GetInfoSession(
		$is_adminka 
			? 
			(isset($_COOKIE[SESS_NAME]) ? $_COOKIE[SESS_NAME] : '')
			 : 
			auth_UserSessId()
		);
	$p_is_root = preg_match('#'.ROOT_PLACE.'#',CURRENT_QUERY);
	
	if ($p_is_root)
		return (int)$array['user_id'];
	else 
		return  (int)$array['user_id']>5 ? (int)$array['user_id'] : 0;
	}

function auth_inGroups($groups, $user_id=0)
	{
	global $DB;

	$commin=0;	
	
	if ($user_id==0) $user_id = auth_getUserId();
	
	if (is_numeric($groups) || !is_array($groups)) $groups = array($groups);

	foreach ($groups as $gid)
		{
		$sql='SELECT user_id FROM `'.PRFX.'users` WHERE CONCAT(",",groups,",") LIKE "%,'.$gid.',%" AND user_id="'.(int)$user_id.'"';

		$test = $DB->getOne($sql);
		if ($test == (int)$user_id)
			$commin++;
		}

	if ($commin>0) return true; else return false;
	}

function auth_getUserName($user_id)
	{
	global $DB;

	$data = $DB->getOne('SELECT username FROM mp_users WHERE user_id="'.(int)$user_id.'"');

	return $data;
	}

function auth_userPasswordCheck($user_id, $pass)
	{
	global $DB;

	return $DB->getOne('SELECT 1 FROM mp_users WHERE pass=MD5("'.$DB->pre($pass).'") AND user_id="'.$DB->pre((int)$user_id).'"');
	}
?>