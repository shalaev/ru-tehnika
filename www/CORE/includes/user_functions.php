<?
function getClientPages($page=1,$count,$onpage=10) 
{ 
    $pages=ceil($count/$onpage); 

    $get_pages=Array(); 
    for($i=1;$i<=$pages;$i++) { 
        if ($page<($pages-7) && $pages>10 && $i==($pages-5)) 
            $get_pages[]='space'; 

        if ($i<6 || $i>($pages-5) || $i==$page || ($i>=($page-2) && ($i<$page)) || ($i<=($page+2) && ($i>$page))) 
            $get_pages[]=$i; 

        if ($page>8 && $i==5 && $pages>10) 
            $get_pages[]='space'; 
    } 

    $lim_start=($pages>1)?($page-1)*$onpage:0; 
    $get_limit=' LIMIT '.$lim_start.','.$onpage; 
    return Array($get_limit,$get_pages); 
}

function pageNum($data) 
{
	if (strpos($data,'page')!==false) 
		return (int)str_replace('page','',$data);

	return 1;
}

function pageUrl($page) 
{
	$cur_path=trim(path(getPathId()),'/');
	
	$url = array($cur_path);
	if ((int)getParam(0)>0) $url[] = (int)getParam(0);
	if ((int)getParam(1)>0) $url[] = (int)getParam(1);
	if ((int)getParam(2)>0) $url[] = (int)getParam(2);
	
	$url[]='page'.$page;
	
	return implode('/',$url);
}

function printPages($pages,$_page) 
{
	$_pages=Array();
	foreach($pages as $pg)
	{
	    if ($pg=='space') 
	        $_pages[]='<td class="page">...</td>'; 
	    else if ($_page==$pg) 
	        $_pages[]='<td class="page select">'.$pg.'</td>'; 
	    else 
	        $_pages[]='<td class="page"><a href="/'.pageUrl($pg).'/">'.$pg.'</a></td>';
	        
	    $max_page=$pg;
	}
	
	ob_start();
	if (count($_pages)>1) 
	{
		$url_b = (($_page-1)>0) ? '/'.pageUrl($_page-1).'/' : 'javascript:void(0)';
		$url_f = (($_page+1)<=$max_page) ? '/'.pageUrl($_page+1).'/' : 'javascript:void(0)';
		?>
		<table cellpadding="0" cellspacing="0" border="0" class="pages">
			<tr>
				<td width="85" align="left"><a href="<?=$url_b?>">����������</a></td>
				<?=implode('',$_pages)?>
				<td width="85" align="right"><a href="<?=$url_f?>">���������</a></td>
			</tr>
		</table>
		<?
	}
	
	return ob_get_clean();
}


function getInput($name,$data) {
	if (!is_array($data)||!isset($name)) return '';


	ob_start();

	if (!empty($data['value']))
		$data['value']=htmlspecialchars($data['value']);
	
	switch ($data['type']) {
		case 'text':
			echo '<input type="text" name="'.$name.'" value="'.(isset($data['value'])?$data['value']:'').'" class="inp_text">';
		break;

		case 'textarea':
			echo '<textarea name="'.$name.'">'.(isset($data['value'])?$data['value']:'').'</textarea>';
		break;

		case 'list':
			echo '<select name="'.$name.'" class="inp_text">';
			if (isset($data['values']))
				foreach ($data['values'] as $n=>$v)
					echo '<option value="'.$n.'"'.(isset($data['value'])&&$n==$data['value']?' selected':'').'>'.$v.'</option>';
			echo '</select>';
		break;

		case 'code':
			echo '<input type="text" name="'.$name.'" class="code" class="inp_text"><br><img src="/captcha/">';
		break;
	}

	return ob_get_clean();

}

function getMailersSend($cat=1) {
	global $DB;
	$adm_mails=$DB->getAll("SELECT `mail` FROM `mp_mailer`");

	$res=Array();
	foreach ($adm_mails as $adm)
		$res[]=$adm['mail'];
		
	return implode(';',$res);
}
?>