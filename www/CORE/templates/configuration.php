<table cellpadding="0" cellspacing="0" border="0" class="configuration" align="center">
		<? foreach($configurations as $data) : ?>
			<? if (isset($data['html_value'])) : ?>
				<tr><td class="caption"><?=$data['caption'];?></td><td><?=$data['html_value'];?></td></tr>
			<? else : ?>
				<tr><td colspan="2"><?=$data['caption'];?></td></tr>
			<? endif;?>
		<? endforeach; ?>
</table>
