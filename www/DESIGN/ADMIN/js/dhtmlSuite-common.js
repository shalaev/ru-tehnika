if(!window.DHTMLSuite)var DHTMLSuite = new Object();
if(!String.trim)String.prototype.trim=function(){ return this.replace(/^\s+|\s+$/, ''); };
var DHTMLSuite_funcs=new Object();
if(!window.DHTML_SUITE_THEME)var DHTML_SUITE_THEME='blue';
if(!window.DHTML_SUITE_THEME_FOLDER)var DHTML_SUITE_THEME_FOLDER='/DESIGN/ADMIN/themes/';
var DHTMLSuite=new Object();
var standardObjectsCreated=false;
DHTMLSuite.eventElements=new Array();
DHTMLSuite.discardElement=function(element){
element=DHTMLSuite.commonObj.getEl(element);
var garbageBin=document.getElementById('IELeakGarbageBin');
if (!garbageBin){
garbageBin=document.createElement('DIV');
garbageBin.id='IELeakGarbageBin';
garbageBin.style.display='none';
document.body.appendChild(garbageBin);
}
garbageBin.appendChild(element);
garbageBin.innerHTML='';
}
DHTMLSuite.createStandardObjects=function(){
DHTMLSuite.clientInfoObj=new DHTMLSuite.clientInfo();
DHTMLSuite.clientInfoObj.init();
if(!DHTMLSuite.configObj){
DHTMLSuite.configObj=new DHTMLSuite.config();
DHTMLSuite.configObj.init();
}
DHTMLSuite.commonObj=new DHTMLSuite.common();
DHTMLSuite.vs=new DHTMLSuite.globalVariableStorage();;
DHTMLSuite.commonObj.init();
DHTMLSuite.domQueryObj=new DHTMLSuite.domQuery();
DHTMLSuite.commonObj.addEvent(window,'unload',function(){ DHTMLSuite.commonObj.__clearMemoryGarbage(); });
standardObjectsCreated=true;
}
DHTMLSuite.config=function(){
var imagePath;
var cssPath;
var defaultCssPath;
var defaultImagePath;
}
DHTMLSuite.config.prototype={
init:function()
{
this.imagePath=DHTML_SUITE_THEME_FOLDER+DHTML_SUITE_THEME+'/images/';
this.cssPath=DHTML_SUITE_THEME_FOLDER+DHTML_SUITE_THEME+'/css/';
this.defaultCssPath=this.cssPath;
this.defaultImagePath=this.imagePath;
}
,
setCssPath:function(newCssPath)
{
this.cssPath=newCssPath;
}
,
resetCssPath:function()
{
this.cssPath=this.defaultCssPath;
}
,
resetImagePath:function()
{
this.imagePath=this.defaultImagePath;
}
,
setImagePath:function(newImagePath)
{
this.imagePath=newImagePath;
}
}
DHTMLSuite.globalVariableStorage=function(){
var menuBar_highlightedItems;
this.menuBar_highlightedItems=new Array();
var arrayDSObjects;
var arrayOfDhtmlSuiteObjects;
this.arrayDSObjects=new Array();
this.arrayOfDhtmlSuiteObjects=this.arrayDSObjects;
var ajaxObjects;
this.ajaxObjects=new Array();
}
DHTMLSuite.globalVariableStorage.prototype={
}
DHTMLSuite.common=function(){
var loadedCSSFiles;
var cssCacheStatus;
var eventElements;
var isOkToSelect;
this.okToSelect=true;
this.cssCacheStatus=true;
this.eventElements=new Array();
}
DHTMLSuite.common.prototype={
init:function()
{
this.loadedCSSFiles=new Array();
}
,
loadCSS:function(cssFileName,prefixConfigPath)
{
if(!prefixConfigPath&&prefixConfigPath!==false)prefixConfigPath=true;
if(!this.loadedCSSFiles[cssFileName]){
this.loadedCSSFiles[cssFileName]=true;
var linkTag=document.createElement('LINK');
if(!this.cssCacheStatus){
if(cssFileName.indexOf('?')>=0)cssFileName=cssFileName+'&'; else cssFileName=cssFileName+'?';
cssFileName=cssFileName+'rand='+ Math.random();
}
if(prefixConfigPath){
linkTag.href=DHTMLSuite.configObj.cssPath+cssFileName;
}else{
linkTag.href=cssFileName;
}
linkTag.rel='stylesheet';
linkTag.media='screen';
linkTag.type='text/css';
document.getElementsByTagName('HEAD')[0].appendChild(linkTag);
}
}
,
_sTextSelOk:function(okToSelect){
this.okToSelect=okToSelect;
}
,
__isTextSelOk:function()
{
return this.okToSelect;
}
,
setCssCacheStatus:function(cssCacheStatus)
{
this.cssCacheStatus=cssCacheStatus;
}
,
getEl:function(elRef)
{
if(typeof elRef=='string'){
if(document.getElementById(elRef))return document.getElementById(elRef);
if(document.forms[elRef])return document.forms[elRef];
}
return elRef;
}
,
getStyle:function(inputObj,property)
{
if(inputObj.currentStyle){
if(inputObj.currentStyle[property])return inputObj.currentStyle[property];
}
var compStyle=document.defaultView.getComputedStyle(inputObj,'');
if(compStyle){
var retVal=compStyle[property];
}
return  retVal||inputObj.style[property];
}
,
getLeftPos:function(inputObj)
{
if(document.getBoxObjectFor){
return document.getBoxObjectFor(inputObj).x
}
var returnValue=inputObj.offsetLeft;
while((inputObj=inputObj.offsetParent)!=null){
if(inputObj.tagName!='HTML'){
returnValue += inputObj.offsetLeft;
if(document.all)returnValue+=inputObj.clientLeft;
}
}
return returnValue;
}
,
getTopPos:function(inputObj)
{
if(document.getBoxObjectFor){
return document.getBoxObjectFor(inputObj).y
}
var returnValue=inputObj.offsetTop;
while((inputObj=inputObj.offsetParent)!=null){
if(inputObj.tagName!='HTML'){
returnValue += (inputObj.offsetTop-inputObj.scrollTop);
if(document.all)returnValue+=inputObj.clientTop;
}
}
return returnValue;
}
,
getCookie:function(name){
var start=document.cookie.indexOf(name+"=");
var len=start+name.length+1;
if ((!start)&&(name!=document.cookie.substring(0,name.length)))return null;
if (start==-1)return null;
var end=document.cookie.indexOf(";",len);
if (end==-1)end=document.cookie.length;
return unescape(document.cookie.substring(len,end));
}
,
setCookie:function(name,value,expires,path,domain,secure){
expires=expires*60*60*24*1000;
var today=new Date();
var expires_date=new Date( today.getTime()+(expires));
var cookieString=name+"=" +escape(value)+
((expires)?";expires="+expires_date.toGMTString():"")+
((path)?";path="+path:"")+
((domain)?";domain="+domain:"")+
((secure)?";secure":"");
document.cookie=cookieString;
}
,
deleteCookie:function( name, path, domain )
{
if ( this.getCookie( name ))document.cookie=name+"=" +
(( path )?";path="+path:"")+
(( domain )?";domain="+domain:"" )+
";expires=Thu, 01-Jan-1970 00:00:01 GMT";
}
,
cancelEvent:function()
{
return false;
}
,
addEvent:function( obj, type, fn,suffix ){
if(!suffix)suffix='';
if ( obj.attachEvent ){
if ( typeof DHTMLSuite_funcs[type+fn+suffix]!='function' ){
DHTMLSuite_funcs[type+fn+suffix]=function(){
fn.apply(window.event.srcElement);
};
obj.attachEvent('on'+type, DHTMLSuite_funcs[type+fn+suffix] );
}
obj=null;
} else {
obj.addEventListener( type, fn, false );
}
this._aEEl(obj);
}
,
removeEvent:function(obj,type,fn,suffix)
{
if ( obj.detachEvent ){
obj.detachEvent( 'on'+type, DHTMLSuite_funcs[type+fn+suffix] );
DHTMLSuite_funcs[type+fn+suffix]=null;
obj=null;
} else {
obj.removeEventListener( type, fn, false );
}
}
,
__clearMemoryGarbage:function()
{
if(!DHTMLSuite.clientInfoObj.isMSIE)return;
for(var no=0;no<DHTMLSuite.eventElements.length;no++){
try{
DHTMLSuite.eventElements[no].onclick=null;
DHTMLSuite.eventElements[no].onmousedown=null;
DHTMLSuite.eventElements[no].onmousemove=null;
DHTMLSuite.eventElements[no].onmouseout=null;
DHTMLSuite.eventElements[no].onmouseover=null;
DHTMLSuite.eventElements[no].onmouseup=null;
DHTMLSuite.eventElements[no].onfocus=null;
DHTMLSuite.eventElements[no].onblur=null;
DHTMLSuite.eventElements[no].onkeydown=null;
DHTMLSuite.eventElements[no].onkeypress=null;
DHTMLSuite.eventElements[no].onkeyup=null;
DHTMLSuite.eventElements[no].onselectstart=null;
DHTMLSuite.eventElements[no].ondragstart=null;
DHTMLSuite.eventElements[no].oncontextmenu=null;
DHTMLSuite.eventElements[no].onscroll=null;
DHTMLSuite.eventElements[no]=null;
}catch(e){
}
}
for(var no in DHTMLSuite.vs.arrayDSObjects){
DHTMLSuite.vs.arrayDSObjects[no]=null;
}
window.onbeforeunload=null;
window.onunload=null;
DHTMLSuite=null;
}
,
_aEEl:function(el)
{
DHTMLSuite.eventElements[DHTMLSuite.eventElements.length]=el;
}
,
getSrcElement:function(e)
{
var el;
if (e.target)el=e.target;
else if (e.srcElement)el=e.srcElement;
if (el.nodeType==3)
el=el.parentNode;
return el;
}
,
isObjectClicked:function(obj,e)
{
var src=this.getSrcElement(e);
var string=src.tagName+'('+src.className+')';
if(src==obj)return true;
while(src.parentNode&&src.tagName.toLowerCase()!='html'){
src=src.parentNode;
string=string+','+src.tagName+'('+src.className+')';
if(src==obj)return true;
}
return false;
}
,
getObjectByClassName:function(e,className)
{
var src=this.getSrcElement(e);
if(src.className==className)return src;
while(src&&src.tagName.toLowerCase()!='html'){
src=src.parentNode;
if(src.className==className)return src;
}
return false;
}
,
getObjectByAttribute:function(e,attribute)
{
var src=this.getSrcElement(e);
var att=src.getAttribute(attribute);
if(!att)att=src[attribute];
if(att)return src;
while(src&&src.tagName.toLowerCase()!='html'){
src=src.parentNode;
var att=src.getAttribute('attribute');
if(!att)att=src[attribute];
if(att)return src;
}
return false;
}
,
getUniqueId:function()
{
var no=Math.random()+'';
no=no.replace('.','');
var no2=Math.random()+'';
no2=no2.replace('.','');
return no+no2;
}
,
getAssociativeArrayFromString:function(propertyString)
{
if(!propertyString)return;
var retArray=new Array();
var items=propertyString.split(/,/g);
for(var no=0;no<items.length;no++){
var tokens=items[no].split(/:/);
retArray[tokens[0]]=tokens[1];
}
return retArray;
}
,
correctPng:function(elementReference)
{
elementReference=DHTMLSuite.commonObj.getEl(elementReference);
var img=elementReference;
var width=img.width;
var height=img.height;
var html='<span style="display:inline-block;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\''+img.src+'\',sizingMethod=\'scale\');width:'+width+';height:'+height+'"></span>';
img.outerHTML=html;
}
}
DHTMLSuite.clientInfo=function(){
var browser;
var isOpera;
var isMSIE;
var isOldMSIE;
var isFirefox;
var navigatorVersion;
var isOldMSIE;
}
DHTMLSuite.clientInfo.prototype={
init:function()
{
this.browser=navigator.userAgent;
this.isOpera=(this.browser.toLowerCase().indexOf('opera')>=0)?true:false;
this.isFirefox=(this.browser.toLowerCase().indexOf('firefox')>=0)?true:false;
this.isMSIE=(this.browser.toLowerCase().indexOf('msie')>=0)?true:false;
this.isOldMSIE=(this.browser.toLowerCase().match(/msie [0-6]/gi))?true:false;
this.isSafari=(this.browser.toLowerCase().indexOf('safari')>=0)?true:false;
this.navigatorVersion=navigator.appVersion.replace(/.*?MSIE (\d\.\d).*/g,'$1')/1;
this.isOldMSIE=(this.isMSIE&&this.navigatorVersion<7)?true:false;
}
,
getBrowserWidth:function()
{
if(self.innerWidth)return self.innerWidth;
return document.documentElement.offsetWidth;
}
,
getBrowserHeight:function()
{
if(self.innerHeight)return self.innerHeight;
return document.documentElement.offsetHeight;
}
}
DHTMLSuite.domQuery=function(){
document.getElementsByClassName=this.getElementsByClassName;
document.getElementsByAttribute=this.getElementsByAttribute;
}
DHTMLSuite.domQuery.prototype={
}
