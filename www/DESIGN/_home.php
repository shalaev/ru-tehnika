<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html itemscope itemtype="http://schema.org/Organization">
<head>
	<meta itemprop="name" content="����� ������: ����������� ������� � ����������">
	<meta itemprop="description" content="�������� ��������� �������������� ������������ ������������, Cummins, FG Wilson � ��. �����, � ����� ����-�����������, ������ ����������.">
	<meta itemprop="image" content="http://www.ru-tehnika.ru/DESIGN/SITE/images_new/logo_new_g.png">
	<meta itemprop="address" content="620050, �. ������������, ��. �������������, 22">
	<meta itemprop="telephone" content="+7(343)287-15-20">
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
	<title><?=$_TITLEPAGE;?></title>	
	<meta name="keywords" content="<?=$_META_KEYWORDS?>">
	<meta name="description" content="<?=$_META_DESCRIPTION?>">
	<meta name="copyright" content="��� ������ ������">	
	<meta name="google-site-verification" content="N72Sn4HPzvSaxhc3udpvbUq5nXGeyqqYqsAl3Z8URCg" />
	<meta name="google-site-verification" content="_OSGSXNrQAM_EmCgWA5Ko020Q5GfQEYKKrOSe5Y9qfs" />
	<meta name="google-site-verification" content="ThKuV6AGI5Ha1oZ8p34akn20VywstgV8YSRkP-4v240" />
	<meta name='yandex-verification' content='6824dffa17a18dbb' />
	<meta name="robots" content="noyaca"/>
	<link rel="icon" href="/favicon_new.ico" type="image/x-icon">
	<link rel="shortcut icon" href="/favicon_new.ico" type="image/x-icon"> 		
	<link href="/DESIGN/SITE/style.css" rel="stylesheet" type="text/css" title="default">
	<link rel="stylesheet" type="text/css" href="/DESIGN/SITE/highslide/highslide.css" />
	<script type="text/javascript" src="/DESIGN/SITE/js/script.js"></script>
	<script type="text/javascript" src="/DESIGN/SITE/js/img_popup.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" ></script> 

	<!--
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
	-->	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.5.3/jquery-ui.min.js" ></script>
	
	<script type="text/javascript" src="/DESIGN/SITE/js/menu.js"></script>


<script type="text/javascript">
	$(function () {
		var tabContainers = $('div.tabs > div'); // �������� ������ �����������
		tabContainers.hide().filter(':first').show(); // ������ ���, ����� �������
		// ����� �������������� ���� �� �������
		$('div.tabs ul.tabNavigation a').click(function () {
			tabContainers.hide(); // ������ ��� ����
			tabContainers.filter(this.hash).show(); // ���������� ���������� ��������
			$('div.tabs ul.tabNavigation a').removeClass('selected'); // � ���� ������� ����� 'selected'
			$(this).addClass('selected'); // ������� ������� ��������� ����� 'selected'
			return false;
		}).filter(':first').click();
	});
	
 $(document).ready(function(){
			   //�-� offset ���������� ���������� ���-�� ������ ��������	
	var sidebartop = $('#contx').offset().top;	
				
	$(window).scroll(function(){
		if( $(window).scrollTop() > sidebartop ) 
		{ // �-� scrollTop() ���������� �������� ������������� ����������
			$('#contx').css({background: 'url("/DESIGN/SITE/images/transparent2.png")', position: 'fixed', top: '0', marginLeft: '60.2%', padding: '0px 10px 0px 0px'});
			$('.head').css({position: 'static', height: '100px'});
			$('.addr-top').css({position: 'static', display: 'none'});
			$('.head-floater').css({display: 'block', opacity: '0.6'});	
		}
		else 
		{
			$('#contx').css({background: 'none', position: 'static', marginBottom: '-2.6%', padding: '0px'});
			$('.addr-top').css({position: 'static', display: 'block'});		
			$('.head-floater').css({display: 'none'});
		}				
	});
});
	
</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js">
  {lang: 'ru'}
</script>
<script type="text/javascript" src="/DESIGN/SITE/highslide/highslide-with-html.js"></script>
<script type="text/javascript">
   // Apply the Highslide settings
   hs.graphicsDir = '/DESIGN/SITE/highslide/graphics/';
   hs.outlineType = 'rounded-white';
   hs.outlineWhileAnimating = true;
</script>
	<!-- ���� � ��� ������ ��! -->
	<!--[if lte IE 6]>
	<script type="text/javascript" src="/DESIGN/SITE/js/iepngfix_bg.js"></script>
	<style type="text/css">
		.left_menu table .select .ind,
		.top_menu .left,
		.top_menu .right 
		{ 
			behavior: url(/DESIGN/SITE/js/iepngfix.htc) 
		}
	</style>	
	<![endif]-->	
	<!-- toodoo-key: JRC9NfsEcK0JMCVVh1pxC -->
</head>

<body>
<!-- ClickTale Top part -->
<script type="text/javascript">
var WRInitTime=(new Date()).getTime();
</script>
<!-- ClickTale end of Top part -->

<div id="img_full"></div>
<div id="container">
<div align="center">

<table cellpadding="0" cellspacing="0" border="0" style="width:940px;height:100%">
	<tr class="white_row">
		<td colspan="2" valign="top">
		
			<table cellpadding="0" cellspacing="0" border="0" style="width:940px;">
				<tr style="height:100px;">

					<td colspan="5" class="header">
						<div id="header_menu">
							<a href="/"><img src="/DESIGN/SITE/images/header/home.gif" alt=""></a>
							<a rel="nofollow" href="http://www.ru-tehnika.ru/contacts/"><img src="/DESIGN/SITE/images/header/email.gif" alt=""></a>
							<a href="http://www.ru-tehnika.ru/sitemap/"><img src="/DESIGN/SITE/images/header/map.gif" alt=""></a>
						</div>
						<div style="float:left; margin-bottom: -20px; margin-top: 20px;">
							<a href="/"><img border="0" src="/DESIGN/SITE/images_new/logo_new_g.png" alt="����� ������ &mdash; ����������������, ��������������, ����-����������" /></a><br />
							<div style="margin-top:0px;margin-bottom:9px;"><span><strong>����������������, ��������������, ����-����������</strong></span></div>
						</div>
						<div style="clear:both;"></div>
						
						<div class="head">
							<div id="contx">
								<span><img src="/DESIGN/SITE/images/tel2.png" style="margin-bottom:-34px;padding-right:176px;"><div id="ya-phone-1">+7(495)225-40-26</div></span>
								<span><a rel="nofollow" href="http://www.ru-tehnika.ru/to_tel">�������� ������</a></span>
								<span class="head-floater"></span>
							</div>
						<br>
						<p class="addr-top">129085, ������, ��.����, 101, ��. 402</span></p>
					</td>
				</tr>

				<tr style="height:65px;">
					<td><img src="/DESIGN/SITE/images/spacer.gif" border="0" alt=""></td>
					<td class="left_menu" valign="top">
					<div style="width:190px;">

						<div style="padding-top:34px;">
						<div style="margin-left:-24px;width:42px;float:left;"><img border="0" src="/DESIGN/SITE/images/spec_1.png" alt="" align="left" /></div>
						<div style="float:right;padding-right:4px;"><a href="http://www.ru-tehnika.ru/special"><img src="/DESIGN/SITE/images/spec_2.png" border="0" alt="" /></a></div>
						</div>
						<div style="clear:both;"></div>	
						<a href="http://www.ru-tehnika.ru/arenda"><img src="http://www.ru-tehnika.ru/DESIGN/SITE/images/arenda/arenda_dgu.png" alt="������ ��������������" /></a>
						<?=$_MENU_LEFT_?>
						<br />
						<div style="padding-left:15px;"><a href="http://www.ru-tehnika.ru/catalog/4/"><img border="0" src="/DESIGN/SITE/images/banner/120x240.gif" alt="���������������� Cummins � �������! &mdash; ��� ������ ������"></a></div>
						<div style="color:#007bb4;" class="title">��������</div><br />
						<a href="http://www.ru-tehnika.ru/catalog/4/"><img border="0" style="padding-bottom:5px;" src="/DESIGN/SITE/images/brand/cummins.jpg" title="Cummins" alt="Cummins"></a>
						<a href="http://www.ru-tehnika.ru/catalog/5/"><img border="0" style="padding-bottom:5px;" src="/DESIGN/SITE/images/brand/fg_wilson.jpg" title="FG Wilson" alt="FG Wilson"></a>
						<a href="http://www.ru-tehnika.ru/catalog/7/"><img border="0" style="padding-bottom:5px;" src="/DESIGN/SITE/images/brand/aksa.jpg" title="Aksa" alt="Aksa"></a>
						<a href="http://www.ru-tehnika.ru/catalog/13/"><img border="0" style="padding-bottom:5px;" src="/DESIGN/SITE/images/brand/sdmo.jpg" title="SDMO" alt="SDMO"></a>
						<a href="http://www.ru-tehnika.ru/catalog/14/"><img border="0" style="padding-bottom:5px;" src="/DESIGN/SITE/images/brand/gesan.jpg" title="Gesan" alt="Gesan"></a>
						<a href="http://www.ru-tehnika.ru/catalog/15/"><img border="0" style="padding-bottom:5px;" src="/DESIGN/SITE/images/brand/caterpillar.jpg" title="Caterpillar" alt="Caterpillar"></a>
		
						<img src="/DESIGN/SITE/images/qr-code.gif" style="padding-left:11px;padding-top:18px;" alt="QR-���" align="center" />
						
						<ul id="social">
							<li><a rel="nofollow" href="https://plus.google.com/u/0/116822930357735204999"></a></li>
							<li><a rel="nofollow" id="youtube" href="http://www.youtube.com/user/ienergo"></a></li>
							<li><a rel="nofollow" id="vkontakte" href="http://vkontakte.ru/inkom_energo"></a></li>
							<li><a rel="nofollow" id="facebook" href="http://www.facebook.com/inkom.energo"></a></li>
							<li><a rel="nofollow" id="twitter" href="http://twitter.com/inkom_energo"></a></li>
						</ul>
					</div>
					</td>
					<td class="shadow_left"><img src="/DESIGN/SITE/images/spacer.gif" border="0" alt=""></td>
					<td valign="top" class="0menu">
						<?=$_MENU_TOP_?>
						
						<table style="opacity: 0; top: -50px; left: -33px; display: none;" id="dpop" class="item_1_1">
							<tr>
								<td width="214" valign="top">
									<div class="item_1_1-div">
										<ul>
											<li class="item_1_1-name">� ������������ ����� ������ �</li>
											<li class="item_1_1-p"><b><a href="http://www.ru-tehnika.ru/catalog/6/" class="glossify_term">�������������� ����� ��Ļ</a></b></li>
											<li class="item_1_1-p"><b><a href="http://www.ru-tehnika.ru/catalog/8/" class="glossify_term">����-���������� ������</a></b></li>
											<li class="item_1_1-p"><b><a href="http://www.ru-tehnika.ru/catalog/11/" class="glossify_term">�����/������ (���, ����, ����)</a></b></li>
											
											<li class="item_1_1-name">� ���������� ������������ �</li>
											<li class="item_1_1-p"><b><a href="http://www.ru-tehnika.ru/catalog/4/" class="glossify_term">�������������� Cummins</a></b></li>
											<li class="item_1_1-p"><b><a href="http://www.ru-tehnika.ru/catalog/5/" class="glossify_term">�������������� FG Wilson</a></b></li>
											<li class="item_1_1-p"><b><a href="http://www.ru-tehnika.ru/catalog/7/" class="glossify_term">�������������� AKSA</a></b></li>
											<li class="item_1_1-p"><b><a href="http://www.ru-tehnika.ru/catalog/13/" class="glossify_term">�������������� SDMO</a></b></li>
											<li class="item_1_1-p"><b><a href="http://www.ru-tehnika.ru/catalog/14/" class="glossify_term">�������������� Gesan</a></b></li>
											<li class="item_1_1-p"><b><a href="http://www.ru-tehnika.ru/catalog/15/" class="glossify_term">�������������� Caterpillar</a></b></li>
										</ul>
									</div>
								</td>
								<td width="285">
								<a href="http://www.ru-tehnika.ru/special"><img src="/DESIGN/SITE/images/banner/ad200_akcia.png" alt="���������������"></a>
								</td>
							</tr>
						</table>
						
						<table cellpadding="0" cellspacing="0" border="0" style="width:800px;">
							<tr>
								<td valign="top">
									<div class="content">
									<?include(BLOCKS_DIR.'/way.php')?>									
										
										<?=(!empty($_TITLEMENU)?'<h1>'.$_TITLEMENU.'</h1>':'')?>
										<?=$_CONTENT_?>
										<?=$_CONTENT_ADD_?>
									</div>								
								</td>
							<?
							$ch = trim(strip_tags($_NEWS_));
							if (!empty($ch)) {?>
								<td width="190" valign="top">
									<?=$_NEWS_?>
								</td>
							<?}?>
							</tr>
						</table>	
					
					</td>
					<td class="shadow_right"><img src="/DESIGN/SITE/images/spacer.gif" border="0" alt=""></td>
				</tr>
			</table>
			
		</td>
	</tr>
	<tr style="height:10px;">
		<td class="bottom_left"><img src="/DESIGN/SITE/images/spacer.gif" border="0" alt=""></td>
		<td class="bottom_right"><img src="/DESIGN/SITE/images/spacer.gif" border="0" alt=""></td>
	</tr>

<tr class="footer" style="height:60px;">                
		<td colspan="2">
<div align="center">
		<ul id="bottom-nav">
			<li><a title="� �������� ������ ������" href="/about/">� ��������</a></li>
			<li><a title="��������������� ������ ������" href="/special/">���������������</a></li>
			<li><a title="��������� ������ ������" href="/catalog/">���������</a></li>
			<li><a rel="nofollow" title="������� ������ ������" href="/news/">�������</a></li>
			<li><a rel="nofollow" title="������, �������� ��������� �� ������ ������" href="/useful/articles/">������</a></li>
			<li><a rel="nofollow" title="�������� ���������� �� ������ ������" href="/useful/">�������� ����������</a></li>
			<li><a rel="nofollow" title="������ � �������� ������ ������" href="/otzyvy/">������</a></li>			
			<li><a rel="nofollow" title="�������� ������ ������" href="/contacts/">��������</a></li>
		</ul>


</div>
			<div style="float:left">
				<div style="float:left">
					<span style="font-weight: bold; font-size: 13px;">�. ������ <div class="footer-phone" id="ya-phone-2">+7(495)225-40-26</div></span>
				</div>
				<div style="float:left">
					<span style="font-weight: bold; font-size: 13px;">�. ������������ <div class="footer-phone">+7(343)287-15-20</div>
					<a rel="nofollow" style="color: #007BB4;" href="http://www.ru-tehnika.ru/contacts/">����� �������</a></span>
				</div>
				<div style="clear:both"></div>
			<p class="copyright">2006-2012 &copy; ���������������� �������������� �������� &mdash; ��� �<a href="/about">����� ������</a>�</p>
			</div>
			<div style="float:left"><div>
<!--LiveInternet counter-->
<script type="text/javascript"><!--
document.write("<a href='http://www.liveinternet.ru/click' rel='nofollow' "+
"target=_blank><img src='http://counter.yadro.ru/hit?t41.1;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='31' height='31'><\/a>")
//--></script><!--/LiveInternet-->

<!-- begin of Top100 code -->
<script id="top100Counter" type="text/javascript" src="http://counter.rambler.ru/top100.jcn?2504997"></script>
<noscript>
<a rel="nofollow" href="http://top100.rambler.ru/navi/2504997/">
<img src="http://counter.rambler.ru/top100.cnt?2504997" alt="Rambler's Top100" border="0" />
</a>
</noscript>
<!-- end of Top100 code -->

<!-- Top.Elec.Ru Counter -->
<a rel="nofollow" href="http://top.elec.ru/stat/12469/" target="_blank"><script language="JavaScript"><!--
ElNav = navigator; ElNavApp = ElNav.appName; ElDoc = document; ElDoc.cookie = "b=b";
ElCookie = ElDoc.cookie ? 1 : 0; ElNsc = (ElNavApp.substring(0, 2) == "Mi") ? 0 : 1;
ElScreen = screen; ElDepth = (ElNsc ==0 ) ? ElScreen.colorDepth : ElScreen.pixelDepth;
document.write('<img src="http://top.elec.ru/cnt?' + 
'id=12469&sc=26&' + 
'scr=' + ElScreen.width + 'x' + ElScreen.height + 'x' + ElDepth + '&' +
'cookie=' + ElCookie + '&' +
'ref=' + escape(ElDoc.referrer) + '&' + 
'r=' + Math.random() + '" ' +
'width="88" height="31" border="0" alt="Top.Elec.Ru - ������� � ������� ������������������ ��������">');
// --></script><noscript><img src="http://top.elec.ru/cnt?id=12469&sc=26" 
width="88" height="31" border="0" alt="Top.Elec.Ru - ������� � ������� ������������������ ��������"></noscript></a>
<!-- Top.Elec.Ru Counter -->

<!-- ������ ���� �������� UralWeb -->
<script language="JavaScript" type="text/javascript">
<!--
  uralweb_d=document;
  uralweb_a='';
  uralweb_a+='&r='+escape(uralweb_d.referrer);
  uralweb_js=10;
//-->
</script>
<script language="JavaScript1.1" type="text/javascript">
<!--
  uralweb_a+='&j='+navigator.javaEnabled();
  uralweb_js=11;
//-->
</script>
<script language="JavaScript1.2" type="text/javascript">
<!--
  uralweb_s=screen;
  uralweb_a+='&s='+uralweb_s.width+'*'+uralweb_s.height;
  uralweb_a+='&d='+(uralweb_s.colorDepth?uralweb_s.colorDepth:uralweb_s.pixelDepth);
  uralweb_js=12;
//-->
</script>
<script language="JavaScript1.3" type="text/javascript">
<!--
  uralweb_js=13;
//-->
</script>
<script language="JavaScript" type="text/javascript">
<!--
uralweb_d.write('<a rel="nofollow" href="http://www.uralweb.ru/rating/go/inkomenergo">'+
'<img border="0" src="http://hc.uralweb.ru/hc/inkomenergo?js='+
uralweb_js+'&rand='+Math.random()+uralweb_a+
'" width="88" height="31" alt="������� UralWeb" /><'+'/a>');
//-->
</script>

<noscript>
<a rel="nofollow" href="http://www.uralweb.ru/rating/go/inkomenergo">
<img border="0" src="http://hc.uralweb.ru/hc/inkomenergo?js=0" width="88" height="31" alt="������� UralWeb" /></a>
</noscript>
<!-- ����� ���� �������� UralWeb -->


<!--Google counter-->	
<script type="text/javascript"><!--
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18467961-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
	//-->
</script>
<!--/Google counter-->
		
<!-- Yandex.Metrika counter -->
<script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript"></script>
<script type="text/javascript">
try { var yaCounter646866 = new Ya.Metrika({id:646866, enableAll: true, webvisor:true});}
catch(e) { }
</script>
<noscript><div><img src="//mc.yandex.ru/watch/646866" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Yandex.Metrika Marked Phone --> 
<script type="text/javascript" src="//mc.yandex.ru/metrika/phone.js?counter=646866" defer="defer"></script> 
<!-- /Yandex.Metrika Marked Phone -->

<!-- Analytics.od7.ru -->
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://analytics.od7.ru/" : "http://analytics.od7.ru/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 3);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://analytics.od7.ru/piwik.php?idsite=3" style="border:0" alt="" /></p></noscript>
<!-- End Analytics.od7.ru Code -->

<!-- AWLA -->
<script language="javascript">
awp="";
sg_js="1.0";
sg_r=""+Math.random()+"&r="+escape(document.referrer)+"&pg="+escape(window.location.href);
document.cookie="sg=1; path=/";
sg_r+="&c="+(document.cookie?"1":"0");
if (self!=top) {fr=1;} else {fr=0;}
sg_r+="&fr="+fr;
mt=(new Date()).getTimezoneOffset();
sg_r+="&mt="+mt;
</script>
<script language="javascript1.1">
sg_js="1.1";
sg_r+="&j="+(navigator.javaEnabled()?"1":"0")
</script>
<script language="javascript1.2">
sg_js="1.2";
sg_r+="&wh="+screen.width+'x'+screen.height+"&px="+
(((navigator.appName.substring(0,3)=="Mic"))?
screen.colorDepth:screen.pixelDepth)
</script>
<script language="javascript1.3">sg_js="1.3"</script>
<script language="javascript">
sg_r+="&jv="+sg_js+"&js=1"+"&awp="+awp;
document.write("<a href='http://www.ru-tehnika.ru/'><img "+" src='http://www.ru-tehnika.ru/stat/stat.php?"+sg_r+"&' border=0 width=0 height=0 alt=''></a>")
</script>
<noscript>
<a href="http://www.ru-tehnika.ru/">
<img src="http://www.ru-tehnika.ru/stat/stat.php?nojs=1" border="0" alt="" /></a>
</noscript>
<!-- /AWLA -->
</div>
</div>
			<div align="right" style="float:right; margin-bottom: -6px;">
				<span><a target="_blank" rel="nofollow"  href="http://www.site-robot.ru/">������� ���������� �����:</a></span><br>
				<a target="_blank" rel="nofollow" href="http://www.site-robot.ru/"><img border="0" alt="" src="/DESIGN/SITE/images/siterobot.jpg"></a><br><br>

			</div>
			
			<div class="footer-link"><a href="/">��������� ��������������</a> <span>|</span> <a href="/catalog/8/">����-����������</a> <span>|</span> <a href="/catalog/11/">����� ����������</a></div>
		</td>
</tr>	
	
</table>

</div>
</div>

<!-- ClickTale Bottom part -->
<div id="ClickTaleDiv" style="display: none;"></div>
<script type="text/javascript">
if(document.location.protocol!='https:')
  document.write(unescape("%3Cscript%20src='http://s.clicktale.net/WRd.js'%20type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
if(typeof ClickTale=='function') ClickTale(3145,1,"www14");
</script>
<!-- ClickTale end of Bottom part -->

</body>
</html>