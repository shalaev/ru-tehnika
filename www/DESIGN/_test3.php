<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title><?=$_TITLEPAGE;?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
	<link rel="icon" href="/favicon_new.ico" type="image/x-icon">
	<link rel="shortcut icon" href="/favicon_new.ico" type="image/x-icon"> 
	<link rel="stylesheet" type="text/css" href="/DESIGN/SITE/style2.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/DESIGN/SITE/style_new.css" title="default">
	<meta name="keywords" content="<?=$_META_KEYWORDS?>">
	<meta name="description" content="<?=$_META_DESCRIPTION?>">
	<meta name="google-site-verification" content="N72Sn4HPzvSaxhc3udpvbUq5nXGeyqqYqsAl3Z8URCg" />
	<meta name="google-site-verification" content="_OSGSXNrQAM_EmCgWA5Ko020Q5GfQEYKKrOSe5Y9qfs" />
	<meta name="google-site-verification" content="ThKuV6AGI5Ha1oZ8p34akn20VywstgV8YSRkP-4v240" />
	<meta name='yandex-verification' content='6824dffa17a18dbb' />

	<script type="text/javascript" src="/DESIGN/SITE/js/script.js"></script>
	<script type="text/javascript" src="/DESIGN/SITE/js/img_popup.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" ></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.5.3/jquery-ui.min.js" ></script>
	<script type="text/javascript" src="http://www.ru-tehnika.ru/MODULES/poll/jquery.cookie.js" charset="utf-8"></script>
	<script type="text/javascript" src="http://www.ru-tehnika.ru/MODULES/poll/poll.js" charset="utf-8"></script>

	<script type="text/javascript" src="/DESIGN/SITE/js/jquery.core.js"></script>
	<script type="text/javascript" src="/DESIGN/SITE/js/jquery.superfish.js"></script>
	<script type="text/javascript" src="/DESIGN/SITE/js/jquery.jcarousel.pack.js"></script>
	<script type="text/javascript" src="/DESIGN/SITE/js/jquery.easing.js"></script>
	<script type="text/javascript" src="/DESIGN/SITE/js/jquery.scripts.js"></script>

	<script type="text/javascript">
	$(document).ready(function(){
		$("#featured > ul").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);
	});
	
	$(function () {
		var tabContainers = $('div.tabs > div'); // �������� ������ �����������
		tabContainers.hide().filter(':first').show(); // ������ ���, ����� �������
		// ����� �������������� ���� �� �������
		$('div.tabs ul.tabNavigation a').click(function () {
			tabContainers.hide(); // ������ ��� ����
			tabContainers.filter(this.hash).show(); // ���������� ���������� ��������
			$('div.tabs ul.tabNavigation a').removeClass('selected'); // � ���� ������� ����� 'selected'
			$(this).addClass('selected'); // ������� ������� ��������� ����� 'selected'
			return false;
		}).filter(':first').click();
	});
</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js">
  {lang: 'ru'}
</script>
	<!-- ���� � ��� ������ ��! -->
	<!--[if lte IE 6]>
	<script type="text/javascript" src="/DESIGN/SITE/js/iepngfix_bg.js"></script>
	<style type="text/css">
		.left_menu table .select .ind,
		.top_menu .left,
		.top_menu .right 
		{ 
			behavior: url(/DESIGN/SITE/js/iepngfix.htc) 
		}
	</style>	
	<![endif]-->	
	<!-- toodoo-key: JRC9NfsEcK0JMCVVh1pxC -->
</head>

<body>
<div id="wrap">
    <div class="top_corner"></div>
    <div id="main_container">
    
        <div id="header">
            <div id="logo"><a href="index.html"><img src="/DESIGN/SITE/images_new/logo_new_g.png" alt="" title="" border="0" /></a></div>
            
            <a href="make-a-donation.html" class="make_donation"></a>
            
            <div id="menu">
                <ul>                                                                                            
                    <!--<li><a class="current" href="index.html" title="">���������</a></li>-->
                    <li><a href="#" title="">���������</a></li>
                    <li><a href="#" title="">��� � ������</a></li>
                    <li><a href="#" title="">��������</a></li>
                    <li><a href="contact.html" title="">��������</a></li>
					<li><a href="about.html" title="">������</a></li>
                </ul>
            </div>
            
        
        </div>
        
        
        <div class="middle_banner">               
           
<div class="featured_slider">
      	<!-- begin: sliding featured banner -->
         <div id="featured_border" class="jcarousel-container">
            <div id="featured_wrapper" class="jcarousel-clip">
               <ul id="featured_images" class="jcarousel-list">
                  <li><img src="/DESIGN/SITE/images_new/slider_dgu.jpg" width="965" height="280" alt="" /></li>
                  <li><img src="/DESIGN/SITE/images_new/slider_block_container.jpg" width="965" height="280" alt="" /></li>
                  <li><img src="/DESIGN/SITE/images_new/slider_dgu2.jpg" width="965" height="280" alt="" /></li>
                  <li><img src="/DESIGN/SITE/images_new/slider_skaf_shydg.jpg" width="965" height="280" alt="" /></li>
               </ul>
            </div>
            <div id="featured_positioner_desc" class="jcarousel-container">
               <div id="featured_wrapper_desc" class="jcarousel-clip">
                  <ul id="featured_desc" class="jcarousel-list">                 
                     <li>
                        <div>
                           <p><a href="#"><strong>���������������� ������� ��������������</strong></a><br />
							�������� ��� ������ ������ ���������� ��������� ���������� ����� �� � ���������� ��������� �������������� �� ������� ������� ��������������.
                           </p>
                        </div>
                     </li> 
                     <li>
                        <div>
                           <p><a href="#"><strong>������������ ����-����������� ������</strong></a><br />
							������������ �� ����������� ���������. ����-���������� ����� ��� ���������� �������������� � ������� ���������������� ������������.
                           </p>
                        </div>
                     </li> 
                     <li>
                        <div>
                           <p><a href="#"><strong>� �������� ��� ������ ������</strong></a><br />
							����������� ������������. �������������� ������ � ������� �������. ���������� ������� � ����������� �������� �����!
                           </p>
                        </div>
                     </li>  
                     <li>
                        <div>
                           <p><a href="#"><strong>����� ���������� (���, ����, ����)</strong></a><br />
							�������� ��� ������ ������ ���������� ����� ���������� ������������������, �������������, �������������, ��������.
                           </p>
                        </div>
                     </li> 
                  </ul>
               </div>

            </div>
            <ul id="featured_buttons" class="clear_fix">
               <li>1</li>
               <li>2</li>
               <li>3</li>
               <li>4</li>
            </ul>
         </div>
         <!-- end: sliding featured banner -->
</div>
          
        
        
        </div><!---------------------------------end of middle banner-------------------------------->
        
        
        <div class="center_content">
			<div class="center_text">       
				<h2>������������ � ������� ��������� �������������� (���) � ��������� �����������</h2>
				<p>�������� �<strong><span style="color:#B52025;">�����</span> ������</strong>� ���������� ��������� ���������� ����� �� (<a href="http://ru-tehnika.ru/catalog/6/65/">��-100</a>) � ���������� ��������� �������������� �� ������� ������� �������������� <a href="http://ru-tehnika.ru/catalog/5/">FG Wilson</a>, <a href="http://ru-tehnika.ru/catalog/4/">Cummins</a>, <a href="http://ru-tehnika.ru/catalog/7/">AKSA</a> � ��. ���� �������� ������� �������� �� ����� ������������������ �������������� ������ � 2003 ����.</p>

				<p><img hspace="15" align="left" alt="" src="http://www.ru-tehnika.ru/UPLOAD/user/images/certificat.png"/> ��������� �������� �<strong><span style="color:#B52025;">�����</span> ������</strong>� ��������������� � ������������� ������������� ���������� ��������.</p>
				<p>�� ����������� ���� ��� �<strong><span style="color:#B52025;">�����</span> ������</strong>� �������� ����� �� ���������� �����������, �������������� ������������ � ����������� �������� ������������� ������������ � ��������� �������������� (���) ��� ��������� ������������, ����������������, ���������  ����������� � ������ � ��������.</p>
				<p>�� ����� ����� ����������� ������ ������� ������� � ��������� ����������� �������������.</p>
			</div>
<div style="text-align: center;">
<h3>������� ���������:</h3>

<table cellspacing="0" cellpadding="0" border="0" align="center" width="750" style="font-size: 12px;">
    <tbody>
        <tr>
            <td><a href="http://www.ru-tehnika.ru/catalog/"><img height="145" border="0" width="250" src="http://www.ru-tehnika.ru/DESIGN/SITE/images/slide2/dgu.png" alt=""/></a></td>
            <td><a href="http://www.ru-tehnika.ru/catalog/8/"><img height="145" border="0" width="250" src="http://www.ru-tehnika.ru/DESIGN/SITE/images/slide2/block.png" alt=""/></a></td>
            <td><a href="http://www.ru-tehnika.ru/catalog/11/"><img height="145" border="0" width="250" src="http://www.ru-tehnika.ru/DESIGN/SITE/images/slide2/avto.png" alt=""/></a></td>
        </tr>
        <tr style="background-color: rgb(238, 238, 238); padding: 20px; margin: 20px; text-align: center; height: 30px; border: 1px none rgb(0, 0, 0);">
            <td><a href="http://www.ru-tehnika.ru/catalog/">��������� �������������� <font color="#707273">�</font></a></td>

            <td><a href="http://www.ru-tehnika.ru/catalog/8/">����-���������� <font color="#707273">�</font></a></td>
            <td><a href="http://www.ru-tehnika.ru/catalog/11/">���������� <font color="#707273">�</font></a></td>
        </tr>
    </tbody>
</table>
</div>        
         
        <div class="home_section_left">
            <img src="/DESIGN/SITE/images_new/icon1.gif" alt="" title="" class="home_section_icon" border="0">
                            
                <h2 class="home_title">What we do</h2>
                <div class="home_subtitle">Consectetur adipisicing elit</div>
    
                <div class="home_section_thumb">
                <img src="/DESIGN/SITE/images_new/home_section_thumb1.jpg" alt="" title="" border="0">
                </div>
                <p><span>Lorem ipsum dolor sit ame</span><br>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore. 
                <br> <br>
                <span>Lorem ipsum dolor sit ame</span><br>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore. 
                </p>
                <a href="" class="more"><img src="/DESIGN/SITE/images_new/more.gif" alt="" title="" border="0"></a>
        <div class="clear"></div>
        </div>
        
        
        <div class="home_section_left">
            <img src="/DESIGN/SITE/images_new/icon2.gif" alt="" title="" class="home_section_icon" border="0">
                            
                <h2 class="home_title">Who we are</h2>
                <div class="home_subtitle">Tempor incididunt ut labore</div>
    
                <div class="home_section_thumb">
                <img src="/DESIGN/SITE/images_new/home_section_thumb2.jpg" alt="" title="" border="0">
                </div>
                <p><span>Lorem ipsum dolor sit ame</span><br>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore. 
                <br> <br>
                <span>Lorem ipsum dolor sit ame</span><br>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore. 
                </p>
                <a href="" class="more"><img src="/DESIGN/SITE/images_new/more.gif" alt="" title="" border="0"></a>
        <div class="clear"></div>
        </div>
        
        <div class="home_section_left">
            <img src="/DESIGN/SITE/images_new/icon3.gif" alt="" title="" class="home_section_icon" border="0">
                            
                <h2 class="home_title">Special services</h2>
                <div class="home_subtitle">Sed do eiusmod tempor</div>
    
                <div class="home_section_thumb">
                <img src="/DESIGN/SITE/images_new/home_section_thumb3.jpg" alt="" title="" border="0">
                </div>
                <p><span>Lorem ipsum dolor sit ame</span><br>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore. 
                <br> <br>
                <span>Lorem ipsum dolor sit ame</span><br>
                Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore. 
                </p>
                <a href="" class="more"><img src="/DESIGN/SITE/images_new/more.gif" alt="" title="" border="0"></a>
        <div class="clear"></div>
        </div>
        
            
            <div class="left_block_wide">
                <h2>���� ��������</h2>
                
                <a href="#"><img src="/DESIGN/SITE/images_new/p1.jpg" alt="" title="" border="0" class="projects" /></a>
                <a href="#"><img src="/DESIGN/SITE/images_new/p2.jpg" alt="" title="" border="0" class="projects" /></a>
                <a href="#"><img src="/DESIGN/SITE/images_new/p3.jpg" alt="" title="" border="0" class="projects" /></a>
            
            
            </div>
            
            <div class="right_block">
            	<h2>�������� ��������</h2>
                <p>
                ����� �� ������ �������� �������� �� ���� �������.
                </p>
                <form id="newsletter">
                <input type="text" name="" class="newsletter_input" />
                <input type="submit" name="" class="newsletter_submit" value="�����������" />
                </form>
            
            </div>
        
        
        
   
        
        <div class="clear"></div>
        </div>
        
        <div class="footer">
        	<div class="copyright">����������� ����� � <a href="http://od7.ru/" target="_blank">������� ���������</a></div>
        
        	<div class="footer_links">
                <a class="current" href="index.html" title="">� ��������</a>
                <a href="#" title="">���������</a>
                <a href="#" title="">�������</a>
                <a href="#" title="">������</a>
                <a href="#" title="">��������</a>
                <a href="contact.html" title="">��������</a>            
            </div>
        </div>
      
      
    
    </div>
</div>
</body>
</html>