<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title><?=$_TITLEPAGE;?></title>
	<meta name="keywords" content="<?=$_META_KEYWORDS?>">
	<meta name="description" content="<?=$_META_DESCRIPTION?>">
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
	<link href="/DESIGN/SITE/style_print.css" rel="stylesheet" type="text/css" title="default">
	<link rel="icon" href="/favicon_new.ico" type="image/x-icon">
	<link rel="shortcut icon" href="/favicon_new.ico" type="image/x-icon"> 	
	<script type="text/javascript" src="/DESIGN/SITE/js/script.js"></script>
	<script type="text/javascript" src="/DESIGN/SITE/js/img_popup.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" ></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.5.3/jquery-ui.min.js" ></script>
	<script type="text/javascript" src="http://www.ru-tehnika.ru/MODULES/poll/jquery.cookie.js" charset="utf-8"></script>
	<script type="text/javascript" src="http://www.ru-tehnika.ru/MODULES/poll/poll.js" charset="utf-8"></script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js">
  {lang: 'ru'}
</script>
<script type="text/javascript">
	$(function () {
		var tabContainers = $('div.tabs > div'); // �������� ������ �����������
		tabContainers.hide().filter(':first').show(); // ������ ���, ����� �������
		// ����� �������������� ���� �� �������
		$('div.tabs ul.tabNavigation a').click(function () {
			tabContainers.hide(); // ������ ��� ����
			tabContainers.filter(this.hash).show(); // ���������� ���������� ��������
			$('div.tabs ul.tabNavigation a').removeClass('selected'); // � ���� ������� ����� 'selected'
			$(this).addClass('selected'); // ������� ������� ��������� ����� 'selected'
			return false;
		}).filter(':first').click();
	});
</script>
	<!--[if lte IE 6]>
	<script type="text/javascript" src="/DESIGN/SITE/js/iepngfix_bg.js"></script>
	<style type="text/css">
		.left_menu table .select .ind,
		.top_menu .left,
		.top_menu .right 
		{ 
			behavior: url(/DESIGN/SITE/js/iepngfix.htc) 
		}
	</style>	
	<![endif]-->	
</head>

<body onload="print();">
<div id="container">
<div align="center">

<table cellpadding="0" cellspacing="0" border="0" style="width:700px;height:100%">
	<tr class="white_row">
		<td colspan="2" valign="top">
		
			<table cellpadding="0" cellspacing="0" border="0" style="width:700px;">
				<tr style="height:100px;">
					<td colspan="5" style="border-bottom: 2px solid #73829F;">
						<div style="float:left; margin-bottom: -20px; margin-top: 20px; padding-left: 10px;">
							<a href="/"><img border="0" src="/DESIGN/SITE/images_new/logo_print.png" alt="����� ������ &mdash; ����������������, ��������������, ����-����������" /></a><br />
						</div>
						<div style="float:right;font-size:13px;margin-bottom:-15px;margin-top:19px;padding-right:18px;text-align:right;">
						<p><b>�������� � ������������ ���������������� ������ ������</b><br />
						620088, �. ������������, ��. �������������, 22<br />
						<b>���:</b> (343) 351-76-25<br />
						<b>E-mail:</b> dgu@master-elec.ru <b>Web:</b> <a href="http://www.master-elec.ru">www.master-elec.ru</a></p>
						</div>
						
						<div style="clear:both;"></div>
						<p style="text-align:right;color:#333;font-size:11px;font-weight:bold;padding-right:30px;text-align:right;">������ ��� ������</p>
					</td>
				</tr>

				<tr style="height:65px;">
					<td class="shadow_left"><img src="/DESIGN/SITE/images/spacer.gif" border="0" alt=""></td>				
					<td valign="top">					
						<table cellpadding="0" cellspacing="0" border="0" style="width:675px;">
							<tr>
								<td valign="top">
									<div class="content">									
										<?=(!empty($_TITLEMENU)?'<h1>'.$_TITLEMENU.'</h1>':'')?>
										<?=$_CONTENT_?>
										<?=$_CONTENT_ADD_?>
									</div>								
								</td>
							</tr>
						</table>	
					</td>
					<td class="shadow_right"><img src="/DESIGN/SITE/images/spacer.gif" border="0" alt=""></td>					
				</tr>
			</table>
			
		</td>
	</tr>

<tr class="footer" style="height:60px;">                
		<td colspan="2">

<!-- Google Analytics openstat code start -->
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18467961-1']);
    var OpenStatParser = {
        _params: {},
        _parsed: false,
        _decode64: function(data) {
            if (typeof window['atob'] === 'function') {
                return atob(data);
            }

            var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
            var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
                    ac = 0,
                    dec = "",
                    tmp_arr = [];

            if (!data) {
                return data;
            }

            data += '';

            do {
                h1 = b64.indexOf(data.charAt(i++));
                h2 = b64.indexOf(data.charAt(i++));
                h3 = b64.indexOf(data.charAt(i++));
                h4 = b64.indexOf(data.charAt(i++));

                bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;

                o1 = bits >> 16 & 0xff;
                o2 = bits >> 8 & 0xff;
                o3 = bits & 0xff;

                if (h3 == 64) {
                    tmp_arr[ac++] = String.fromCharCode(o1);
                } else if (h4 == 64) {
                    tmp_arr[ac++] = String.fromCharCode(o1, o2);
                } else {
                    tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
                }
            } while (i < data.length);

            dec = tmp_arr.join('');

            return dec;
        },
        _parse: function() {
            var prmstr = window.location.search.substr(1);
            var prmarr = prmstr.split('&');
            this._params = {};

            for (var i = 0; i < prmarr.length; i++) {
                var tmparr = prmarr[i].split('=');
                this._params[tmparr[0]] = tmparr[1];
            }

            this._parsed = true;
        },
        hasMarker: function() {
            if (!this._parsed) {
                this._parse();
            }
            return (typeof this._params['_openstat'] !== 'undefined') ? true : false;
        },
        buildCampaignParams: function() {
            if (!this.hasMarker()) {
                return false;
            }
            var openstat = this._decode64(this._params['_openstat']);
            var statarr = openstat.split(';');
            return 'utm_campaign=' + statarr[3] + '&utm_source=' + statarr[0] + '&utm_medium=cpc&utm_content=' + statarr[2];
        }
    }
    if (OpenStatParser.hasMarker()) {
        var campaignParams = OpenStatParser.buildCampaignParams();
        if (campaignParams !== false) {
            _gaq.push(['_set', 'campaignParams', campaignParams]);
        }
    }
    _gaq.push(['_addOrganic', 'images.yandex.ru', 'text']);
    _gaq.push(['_addOrganic', 'blogs.yandex.ru', 'text']);
    _gaq.push(['_addOrganic', 'video.yandex.ru', 'text']);
    _gaq.push(['_addOrganic', 'yandex.ru', 'query']);
    _gaq.push(['_addOrganic', 'go.mail.ru', 'q']);
    _gaq.push(['_addOrganic', 'mail.ru', 'q']);
    _gaq.push(['_addOrganic', 'images.google.ru', 'q']);
    _gaq.push(['_addOrganic', 'maps.google.ru', 'q']);
    _gaq.push(['_addOrganic', 'google.com.ua', 'q']);
    _gaq.push(['_addOrganic', 'rambler.ru', 'words']);
    _gaq.push(['_addOrganic', 'nova.rambler.ru', 'query']);
    _gaq.push(['_addOrganic', 'nova.rambler.ru', 'words']);
    _gaq.push(['_addOrganic', 'gogo.ru', 'q']);
    _gaq.push(['_addOrganic', 'nigma.ru', 's']);
    _gaq.push(['_addOrganic', 'search.qip.ru', 'query']);
    _gaq.push(['_addOrganic', 'webalta.ru', 'q']);
    _gaq.push(['_addOrganic', 'sm.aport.ru', 'r']);
    _gaq.push(['_addOrganic', 'meta.ua', 'q']);
    _gaq.push(['_addOrganic', 'search.bigmir.net', 'z']);
    _gaq.push(['_addOrganic', 'search.i.ua', 'q']);
    _gaq.push(['_addOrganic', 'index.online.ua', 'q']);
    _gaq.push(['_addOrganic', 'web20.a.ua', 'query']);
    _gaq.push(['_addOrganic', 'search.ukr.net', 'q']);
    _gaq.push(['_addOrganic', 'search.ukr.net', 'search_query']);
    _gaq.push(['_addOrganic', 'search.com.ua', 'q']);
    _gaq.push(['_addOrganic', 'search.ua', 'q']);
    _gaq.push(['_addOrganic', 'poisk.ru', 'text']);
    _gaq.push(['_addOrganic', 'go.km.ru', 'sq']);
    _gaq.push(['_addOrganic', 'liveinternet.ru', 'ask']);
    _gaq.push(['_addOrganic', 'gde.ru', 'keywords']);
    _gaq.push(['_addOrganic', 'affiliates.quintura.com', 'request']);
    _gaq.push(['_addOrganic', 'akavita.by', 'z']);
    _gaq.push(['_addOrganic', 'search.tut.by', 'query']);
    _gaq.push(['_addOrganic', 'all.by', 'query']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();
</script>
<!-- Google Analytics openstat code end -->
		
<!-- Yandex.Metrika counter -->
<script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript"></script>
<div style="display:none;"><script type="text/javascript">
try { var yaCounter646866 = new Ya.Metrika({id:646866,
          clickmap:true,
          accurateTrackBounce:true, webvisor:true});}
catch(e) { }
</script></div>
<noscript><div><img src="//mc.yandex.ru/watch/646866" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Yandex.Metrika Marked Phone --> 
<script type="text/javascript" src="//mc.yandex.ru/metrika/phone.js?counter=646866" defer="defer"></script> 
<!-- /Yandex.Metrika Marked Phone -->

<!-- AWLA -->
<script language="javascript">
awp="";
sg_js="1.0";
sg_r=""+Math.random()+"&r="+escape(document.referrer)+"&pg="+escape(window.location.href);
document.cookie="sg=1; path=/";
sg_r+="&c="+(document.cookie?"1":"0");
if (self!=top) {fr=1;} else {fr=0;}
sg_r+="&fr="+fr;
mt=(new Date()).getTimezoneOffset();
sg_r+="&mt="+mt;
</script>
<script language="javascript1.1">
sg_js="1.1";
sg_r+="&j="+(navigator.javaEnabled()?"1":"0")
</script>
<script language="javascript1.2">
sg_js="1.2";
sg_r+="&wh="+screen.width+'x'+screen.height+"&px="+
(((navigator.appName.substring(0,3)=="Mic"))?
screen.colorDepth:screen.pixelDepth)
</script>
<script language="javascript1.3">sg_js="1.3"</script>
<script language="javascript">
sg_r+="&jv="+sg_js+"&js=1"+"&awp="+awp;
document.write("<a href='http://www.ru-tehnika.ru/'><img "+" src='http://www.ru-tehnika.ru/stat/stat.php?"+sg_r+"&' border=0 width=0 height=0 alt=''></a>")
</script><a href="http://www.ru-tehnika.ru/"><img src="http://www.ru-tehnika.ru/stat/stat.php?0.07524196928815907&amp;r=&amp;pg=http%3A//www.master-elec.ru/catalog/6/84/&amp;c=1&amp;fr=0&amp;mt=-360&amp;j=0&amp;wh=1440x900&amp;px=24&amp;jv=1.3&amp;js=1&amp;awp=&amp;" alt="" border="0" height="0" width="0"></a>
<noscript>
<a href="http://www.ru-tehnika.ru/">
<img src="http://www.ru-tehnika.ru/stat/stat.php?nojs=1" border="0" alt="" /></a>
</noscript>
<!-- /AWLA -->
</div>
</div>
</td>
</tr>	
	
</table>

</div>
</div>
</body>
</html>