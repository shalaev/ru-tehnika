<?
// slide
?>
<script src="/DESIGN/SITE/BLOCKS/slide/js/jquery-1.4.4.min.js"></script>
<script src="/DESIGN/SITE/BLOCKS/slide/js/slides.min.jquery.js"></script>
<script>
	$(function(){
		$('#slides').slides({
			preload: true,
			preloadImage: '/DESIGN/SITE/BLOCKS/slide/img/loading.gif',
			play: 5000,
			pause: 2500,
			hoverPause: true,
			animationStart: function(){
				$('.caption').animate({
					bottom:-35
				},100);
			},
			animationComplete: function(current){
				$('.caption').animate({
					bottom:0
				},200);
				if (window.console && console.log) {
					// example return of current slide number
					console.log(current);
				};
			}
		});
	});
</script>
<link rel="stylesheet" href="/DESIGN/SITE/BLOCKS/slide/css/global.css">

<div id="slides">
	<div class="slides_container">
		<div>
			<a href="http://www.flickr.com/photos/jliba/4665625073/" title="145.365 - Happy Bokeh Thursday! | Flickr - Photo Sharing!" target="_blank"><img src="/DESIGN/SITE/BLOCKS/slide/img/01.png" width="1000" height="270" alt="Slide 1"></a>
		</div>
		<div>
			<a href="http://www.flickr.com/photos/stephangeyer/3020487807/" title="Taxi | Flickr - Photo Sharing!" target="_blank"><img src="/DESIGN/SITE/BLOCKS/slide/img/02.png" width="1000" height="270" alt="Slide 2"></a>
		</div>
		<div>
			<a href="http://www.flickr.com/photos/childofwar/2984345060/" title="Happy Bokeh raining Day | Flickr - Photo Sharing!" target="_blank"><img src="/DESIGN/SITE/BLOCKS/slide/img/03.png" width="1000" height="270" alt="Slide 3"></a>
		</div>
		<div>
			<a href="http://www.flickr.com/photos/b-tal/117037943/" title="We Eat Light | Flickr - Photo Sharing!" target="_blank"><img src="/DESIGN/SITE/BLOCKS/slide/img/04.png" width="1000" height="270" alt="Slide 4"></a>
		</div>
		<div>
			<a href="http://www.flickr.com/photos/bu7amd/3447416780/" title="&ldquo;I must go down to the sea again, to the lonely sea and the sky; and all I ask is a tall ship and a star to steer her by.&rdquo; | Flickr - Photo Sharing!" target="_blank"><img src="/DESIGN/SITE/BLOCKS/slide/img/05.png" width="1000" height="270" alt="Slide 5"></a>
		</div>
	</div>
	<a href="#" class="prev"><img src="/DESIGN/SITE/BLOCKS/slide/img/arrow-prev.png" width="24" height="43" alt="Arrow Prev"></a>
	<a href="#" class="next"><img src="/DESIGN/SITE/BLOCKS/slide/img/arrow-next.png" width="24" height="43" alt="Arrow Next"></a>
</div>
<div style="clear:both;"></div>