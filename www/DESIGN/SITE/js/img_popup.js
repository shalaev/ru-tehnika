// Yamaha-252; 12.04.08;


function ClientWidth() {
	return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientWidth://document.body.clientWidth;
	600;
}

function ClientHeight() {
	return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientHeight://document.body.clientHeight;
	844;
}	

function CLWidth() {
	return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientWidth:document.body.clientWidth;
}

function CLHeight() {
	return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientHeight:document.body.clientHeight;
}	

function ScrollLeft() {
	return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.scrollLeft:document.body.scrollLeft;
}

function ScrollTop() {
	return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.scrollTop:document.body.scrollTop;
}	

function loadImage(zone,img,imgWidth,imgHeight) {
	cwidth=ClientWidth();
	cheight=ClientHeight();
	c2width=CLWidth();
	c2height=CLHeight();

	widthPrecent=0;
	if (imgWidth>cwidth) {
		rzc=(imgWidth-cwidth)+50;
		prc=imgWidth/100;
		widthPrecent=Math.ceil(rzc/prc);
	}

	heightPrecent=0;
	if (imgHeight>cheight) {
		rzc=(imgHeight-cheight)+50;
		prc=imgHeight/100;
		heightPrecent=Math.ceil(rzc/prc);
	}

	if (widthPrecent>0 || heightPrecent>0) {
		if (widthPrecent>heightPrecent) {
			prec=widthPrecent;
		} else {
			prec=heightPrecent;
		}
		
		imgWidth=Math.ceil(imgWidth-((imgWidth/100)*prec));
		imgHeight=Math.ceil(imgHeight-((imgHeight/100)*prec));
	}	


	resLeft=((c2width-imgWidth)/2)+ScrollLeft();
	resTop=((c2height-imgHeight)/2)+ScrollTop();

	box=document.getElementById(zone);
	box.style.position='absolute';
	box.style.top=resTop+'px';
	box.style.left=resLeft+'px';

	box.style.cursor='pointer';
	box.style.display='block';
	box.style.width=imgWidth;
	box.style.height=imgHeight;
	box.innerHTML='';
	
	box.onclick = function() { 
		box.style.display='none';
		box.innerHTML='';
	}

	image=document.createElement("img");	
	image.src=img;
	image.width=imgWidth;
	image.height=imgHeight;
	image.style.display='none';

	checkimage(image);
	
	box.appendChild(image);
}


function checkimage() {
	if(image.complete)
		image.style.display = 'block';
	else 
		setTimeout("checkimage()",100);
}